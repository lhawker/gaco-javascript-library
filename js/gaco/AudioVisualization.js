////////////////////////////////////////////////////////////////////////////////
// GACO Library
// Uses the Web Audio API
// Audio Visualization
// https://developer.mozilla.org/en-US/docs/Web/API/Web_Audio_API
// HELP LINKS:
// Could be good to have a read of: http://chimera.labs.oreilly.com/books/1234000001552/ch05.html
// http://0xfe.blogspot.co.uk/2011/08/web-audio-spectrum-analyzer.html
// http://books.google.co.uk/books?id=6U7Y1gCi-5IC&pg=PA55&lpg=PA55&dq=requestAnimationFrame+audio+play+back&source=bl&ots=inEnKpHDVW&sig=2rzYs5Z__w2g8WaXoWOeQxK9if8&hl=en&sa=X&ei=jN0WVKe1DKmw7Aa2m4GoBQ&ved=0CEIQ6AEwBA#v=onepage&q=requestAnimationFrame%20audio%20play%20back&f=false
// helpful api info http://creativejs.com/resources/web-audio-api-getting-started/
////////////////////////////////////////////////////////////////////////////////
var GACO = GACO || {};
//GACO.Maps = function(selector) {
GACO.AudioVisualization = function (selector, audio) {
	var _public = {
		//-------------------------------------------
		// Public stop button
		//-------------------------------------------
		IS_PLAYING: function(e){
			_private.isPlaying();
		},
	};

	var _private = {
		//-------------------------------------------
		// Variables
		//-------------------------------------------

		CONTROL_SELECTOR: null,
		initialized: null,
		audio: null,
		audioContext: null,
		analyser: null,
		analyserTwo: null,
		source: null,
		frequencyData: null,
		frequencyDataTwo: null,
		mediaStreamSource: null,
		renderer: null,
		running: null,
		
		//-------------------------------------------
		// Init
		//-------------------------------------------
		init: function() {
			window.AudioContext = window.AudioContext || window.webkitAudioContext || window.mozAudioContext;
			this.initialized = false;
			this.CONTROL_SELECTOR = document.getElementById(selector);
			this.audio = document.getElementById(audio);
			
			this.audioContext = new AudioContext();
			this.analyser = this.audioContext.createAnalyser();
			this.analyser.fftSize = 64;
			
			this.source = this.audioContext.createMediaElementSource(this.audio);
			this.source.connect(this.analyser);
			this.analyser.connect(this.audioContext.destination);
			this.frequencyData = new Uint8Array(this.analyser.frequencyBinCount);
			
			this.renderer = this.setRendererInit();
			this.start();
			this.createListeners();

		},

		//-------------------------------------------
  		// Create Event Listeners
  		//-------------------------------------------
		createListeners: function(){
			this.CONTROL_SELECTOR.querySelector(".button").addEventListener("click", function(e) { 
	        	_private.isPlaying();
	      	});
		},

		//-------------------------------------------
  		// set Renderer Init
  		//-------------------------------------------
		setRendererInit:function(){
			this.initialized = true;
			return this.initialized;
		},

		//-------------------------------------------
		// Check if is playing
		//-------------------------------------------
		isPlaying: function(e) {
			if(_private.running){ 
				_private.stop();
			} else {
				_private.start();
			}
		},

		//-------------------------------------------
		// Start audio playback 
		//-------------------------------------------
		start: function(){
			this.audio.play();
			this.running = true;
			this.renderFrame();
		},

		//-------------------------------------------
		// Stop audio playback 
		//-------------------------------------------
		stop: function(){
			this.running = false;
			this.audio.pause();
			this.CONTROL_SELECTOR.querySelector(".button").innerHTML = "Play";
		},

		//-------------------------------------------
		// Render Frames
		//-------------------------------------------
		renderFrame: function(){
			if (this.running) {
				this.analyser.getByteFrequencyData(this.frequencyData);
				this.CONTROL_SELECTOR.querySelector(".currentTime").innerHTML = parseFloat(this.audio.currentTime).toFixed(2);
				this.CONTROL_SELECTOR.querySelector(".frequencyData p").innerHTML =  Array.prototype.slice.call(this.frequencyData).join(",");
				this.CONTROL_SELECTOR.querySelector(".button").innerHTML = "Stop";
				this.AnimationFrameRequest();
			}
		},

		//-------------------------------------------
		// Frame animation request
		//-------------------------------------------
		AnimationFrameRequest: function(){
			requestAnimationFrame(_private.renderFrame.bind(this));
		},

	};

	_private.init();
	return _public;
};