////////////////////////////////////////////////////////////////////////////////
// GACO Library
// Polyfills to bring IE8 (in particular) up to date
// Include this file if you want the following to work in IE8:
// - addEventListener, removeEventListener
////////////////////////////////////////////////////////////////////////////////

var GACO = GACO || {};
 
GACO.Polyfills = (function(url) {
 
    "use strict";
     
    var _public = {
 
    };
 
    var _private = {
 
        //------------------------------------------------
        // Variables
        //------------------------------------------------
        oListeners: {},
        oEvent: null,
 
        //------------------------------------------------
        // Create all polyfills
        //------------------------------------------------
        init: function() {
            this.unifyRequestAnimationFrame();
            this.addDateNow();
            this.addArrayIndexOf();
        },
 
        //------------------------------------------------
        // Adds requestAnimationFrame where missing
        // https://gist.github.com/paulirish/1579671
        //------------------------------------------------
        unifyRequestAnimationFrame: function() {
            var lastTime = 0;
            var vendors = ['ms', 'moz', 'webkit', '0'];
            for (var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
                window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
                window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame'] || window[vendors[x]+'CancelRequestAnimationFrame'];
            }
            if (!window.requestAnimationFrame) {
                window.requestAnimationFrame = function(callback, element) {
                    var currTime = new Date().getTime();
                    var timeToCall = Math.max(0, 16 - (currTime - lastTime));
                    var id = window.setTimeout(function() { callback(currTime + timeToCall); }, timeToCall);
                    lastTime = currTime + timeToCall;
                    return id;
                };
            }
            if (!window.cancelAnimationFrame) {
                window.cancelAnimationFrame = function(id) {
                    clearTimeout(id);
                }
            }
        },
 
        //------------------------------------------------
        // Adds missing Date.now function in IE8
        // http://afuchs.tumblr.com/post/23550124774/date-now-in-ie8
        //------------------------------------------------
        addDateNow: function() {
            if (!Date.now) {
                Date.now = function() {
                    return (new Date).valueOf();
                }
            }
        },
 
        //------------------------------------------------
        // Array.indexOf if it doesn't already exist
        //------------------------------------------------
        addArrayIndexOf: function() {
            if (!Array.prototype.indexOf) {
                Array.prototype.indexOf = function(obj, start) {
                    var i = (start || 0), length = this.length;
                    for (; i < length; i++) {
                        if (this[i] === obj) return i;
                    }
                    return -1;
                }
            }
        }
    };
 
    _private.init();
    return _public;
}());
 
//------------------------------------------------
// addEventListener and removeEventListener
// http://www.jonathantneal.com/blog/polyfills-and-prototypes/
//------------------------------------------------
if (!this.addEventListener && this.Element) {
    (function () {
        function addToPrototype(name, method) {
            Window.prototype[name] = HTMLDocument.prototype[name] = Element.prototype[name] = method;
        }
      
        var registry = [];
      
        addToPrototype("addEventListener", function (type, listener) {
            var target = this;
      
            registry.unshift({
                __listener: function (event) {
                event.currentTarget = target;
                event.pageX = event.clientX + document.documentElement.scrollLeft;
                event.pageY = event.clientY + document.documentElement.scrollTop;
                event.preventDefault = function () { event.returnValue = false };
                event.relatedTarget = event.fromElement || null;
                event.stopPropagation = function () { event.cancelBubble = true };
                event.relatedTarget = event.fromElement || null;
                event.target = event.srcElement || target;
                event.timeStamp = +new Date;
      
                listener.call(target, event);
                },
                listener: listener,
                target: target,
                type: type
            });
            this.attachEvent("on" + type, registry[0].__listener);
        });
 
        addToPrototype("removeEventListener", function (type, listener) {
            for (var index = 0, length = registry.length; index < length; ++index) {
                if (registry[index].target == this && registry[index].type == type && registry[index].listener == listener) {
                    return this.detachEvent("on" + type, registry.splice(index, 1)[0].__listener);
                }
            }
        });
 
        addToPrototype("dispatchEvent", function (eventObject) {
            try {
                return this.fireEvent("on" + eventObject.type, eventObject);
            } catch (error) {
                for (var index = 0, length = registry.length; index < length; ++index) {
                    if (registry[index].target == this && registry[index].type == eventObject.type) {
                        registry[index].call(this, eventObject);
                    }
                }
            }
        });
    }());
}
