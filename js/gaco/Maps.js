////////////////////////////////////////////////////////////////////////////////
// GACO Library
// Google Maps using the API version 3
// https://maps.googleapis.com/maps/api/js?v=3.exp&amp;language=en
//
// - Selector should have the data attribute 'data-lng' with a numerical longitude.
// - Selector should have the data attribute 'data-lat' with a numerical latitude.
// - Selector should have the data attribute 'data-zoom' with a numerical value.
// - Selector should have the data attribute 'data-mapType' with a valid map type.
// - Selector should have the data attribute 'data-overlayImg' URL string of overlay image.
// - Selector should have the data attribute 'data-blBoundLat' image overlay bottom left bound, numerical latitude.
// - Selector should have the data attribute 'data-blBoundLng' image overlay bottom left bound, numerical latitude.
// - Selector should have the data attribute 'data-trBoundLat' image overlay top left bound, numerical latitude.
// - Selector should have the data attribute 'data-trBoundLng' image overlay top left bound, numerical latitude.
// - add more info as we develop this!!!!!
// Maps
////////////////////////////////////////////////////////////////////////////////

// *********************** TO DO ADD IMAGE OVERLAY FUNCTIONALITY ************
// *********************** READ: https://developers.google.com/maps/documentation/javascript/examples/overlay-simple
var GACO = GACO || {};

GACO.Maps = function(selector) {

	"use strict";

	var _public = new GACO.EventDispatcher();

	var _private = {
		//------------------------------------------------
		// Variables
		//------------------------------------------------
		SELECTOR: null,
		centerLat: null,
		centerLng: null,
		defaultCenterPoint: null,
		zoom: null,
		mapType: null,
		mapOptions: null,
		map: null,
		locations: null,
		markerArray: [],
		infowindow: null,
		groundOverlay: null,
		bottomLeftImageBoundLat: null,
		bottomLeftImageBoundLng: null,
		topRightImageBoundLat: null,
		topRightImageBoundLng: null,


		//------------------------------------------------
		// Init
		//------------------------------------------------
		init: function() {
			this.SELECTOR = GACO.Utils.getDOMElement(selector);
			
			// check if we have the google object
			if (typeof google !== 'undefined') {
				// call functions
   				this.setMapVariables();
				this.mapCenterPoint();
				this.findMarkers();
				this.initializeMap();
				this.createListeners();
			} else {
				alert("Google Map API dependency needed");
			}
		},

		//------------------------------------------------
		// Map center point
		//------------------------------------------------
		setMapVariables: function(){
			this.centerLat = this.SELECTOR.getAttribute("data-lat");
			this.centerLng = this.SELECTOR.getAttribute("data-lng");
			this.zoom = this.SELECTOR.getAttribute("data-zoom");
			this.mapType = this.SELECTOR.getAttribute("data-mapType");
			this.groundOverlay = this.SELECTOR.getAttribute("data-overlayImg");
			this.bottomLeftImageBoundLat = this.SELECTOR.getAttribute("data-blBoundLat");
			this.bottomLeftImageBoundLng = this.SELECTOR.getAttribute("data-blBoundLng");
			this.topRightImageBoundLat = this.SELECTOR.getAttribute("data-trBoundLat");
			this.topRightImageBoundLng = this.SELECTOR.getAttribute("data-trBoundLng");
		},
		//------------------------------------------------
		// Map center point
		//------------------------------------------------
		mapCenterPoint: function() {
			this.defaultCenterPoint = new google.maps.LatLng(this.centerLat, this.centerLng);
		},

		//------------------------------------------------
		// Find markers
		//------------------------------------------------
		findMarkers: function() {
			// get all markers
			var markerItems = this.SELECTOR.querySelectorAll("li"),
				lat, lng, contentString, icon;
			// build array and push to markerArray
			if(markerItems.length > 0) {
				for (var i = 0; i < markerItems.length; i++) { 
					lat = markerItems[i].getAttribute("data-lat");
					lng = markerItems[i].getAttribute("data-lng");
					
					if(markerItems[i].getAttribute("data-icon") === "") {
						icon = "https://maps.gstatic.com/mapfiles/ms2/micons/red-dot.png";
					} else {
						icon = markerItems[i].getAttribute("data-icon");
					}

					contentString = markerItems[i].innerHTML.replace(/\,/g,'&#44;');
					this.markerArray.push([lat, lng, contentString, icon]);	
				} 
			}
		},

		//------------------------------------------------
		// Add a image ground overlay to the map
		//------------------------------------------------
		addOverlay: function() {
			var imageBounds;
			imageBounds = new google.maps.LatLngBounds(
      			new google.maps.LatLng(this.bottomLeftImageBoundLat, this.bottomLeftImageBoundLng),
      			new google.maps.LatLng(this.topRightImageBoundLat, this.topRightImageBoundLng)
      		);
      		this.historicalOverlay = new google.maps.GroundOverlay(this.groundOverlay, imageBounds);
			this.historicalOverlay.setMap(this.map);
		},

		//------------------------------------------------
		// Add markers to the map
		//------------------------------------------------
		addMarkersToMap: function(){
			
			this.infowindow = new google.maps.InfoWindow();

			if(this.markerArray.length > 0) {
				for(var i = 0; i < this.markerArray.length; i++ ) {
					// loop over all markers and add them.
					var marker = new google.maps.Marker({
				        position: new google.maps.LatLng(this.markerArray[i][0], this.markerArray[i][1]),
				        map: this.map,
				        icon: this.markerArray[i][this.markerArray[i].length-1]
				    });

				    // add event listener
				    google.maps.event.addListener(marker, 'click', (function(marker, i) {
			        	return function() {
				          _private.infowindow.setContent(_private.markerArray[i][2]);
				          _private.infowindow.open(_private.map, marker);
				        }
			      	})(marker, i)); 
				}
			}
		},
		
		//------------------------------------------------
		// initialize the map
		//------------------------------------------------
		initializeMap: function(){
			
			this.mapOptions = {
				zoom: parseInt(this.zoom),
				center: this.defaultCenterPoint,
				mapTypeId: google.maps.MapTypeId.ROADMAP
			};

			this.map = new google.maps.Map(
				    this.SELECTOR,
				    this.mapOptions
			);
	
			// add ground overlay
			this.addOverlay();
			// add markers
			this.addMarkersToMap();	
		},

		//------------------------------------------------
		// Window resized, scale carousel
		//------------------------------------------------
		onResizeWindow: function() {},

		//------------------------------------------------
		// Listen for interaction
		//------------------------------------------------
		createListeners: function() {}
	};

	_private.init();
	return _public;
};