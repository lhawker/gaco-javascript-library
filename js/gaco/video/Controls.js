////////////////////////////////////////////////////////////////////////////////
// GACO Library
// Controls for any video player
// This expects controls-container (selector) to cover the whole video, and for
// .controls to be positioned absolutely, with bottom: 0
// Passed selector may contain:
// - .play-button
// - .progress-bar (containing .progress, .loaded, .hitarea, optionally .handle)
// - .mute-button
// - .full-screen-button
// - .video-overlay (clickable surface ontop of video for play/pausing)
// - .controls (if any kind of auto-hiding is desired)
// - options object can contain the following (defaults shown):
// {
// 		fade: false,  // Controls will hide via fade
//		slide: false, // Controls will hide by sliding out of view
//		delay: 1000,  // Time before controls start to hide
//		duration: 300 // Time hide animation takes,
//		mobileMode: false // Does stuff like forcing default controls for mobiles/tablets
// }
////////////////////////////////////////////////////////////////////////////////
GACO.Utils.createNamespace("GACO.video");

GACO.video.Controls = function(selector, options) {

	"use strict";
	
	var _public = new GACO.EventDispatcher();

	//------------------------------------------------
	// Events
	//------------------------------------------------
	_public.PLAY_CLICKED = "PlayClicked";
	_public.PAUSE_CLICKED = "PauseClicked";
	_public.MUTE_CLICKED = "MuteClicked";
	_public.UNMUTE_CLICKED = "UnMuteClicked";
	_public.ENTER_FULL_SCREEN_CLICKED = "EnterFullScreenClicked";
	_public.EXIT_FULL_SCREEN_CLICKED = "ExitFullScreenClicked";
	_public.PROGRESS_CLICKED = "ProgressClicked";
	_public.HANDLE_MOUSE_DOWN = "HandleMouseDown";
	_public.HANDLE_DRAGGED = "HandleDragged";
	_public.HANDLE_RELEASED = "HandleReleased";
	_public.OVERLAY_HIDDEN = "OverlayHidden";

	//------------------------------------------------
	// Set play progress percentage
	//------------------------------------------------
	_public.setProgress = function(percentage) {
		if (!_private.handleDragging) {
			if (_private.progressBar) {
				_private.progressBar.setProgress(percentage);
			}
			if (_private.SELECTOR.querySelector(".handle")) {
				_private.SELECTOR.querySelector(".handle").style.left = (100 * percentage) + "%";
			}
		}
	};

	//------------------------------------------------
	// Set loaded percentage
	//------------------------------------------------
	_public.setLoaded = function(percentage, adjustHitArea) {
		if (_private.progressBar) {
			_private.progressBar.setLoaded(percentage, adjustHitArea);
		}
	};

	//------------------------------------------------
	// Get the percentage the user wants to scan to
	//------------------------------------------------
	_public.getRequestedPercentage = function() {
		if (_private.progressBar) {
			return _private.progressBar.getRequestedPercentage();
		} else {
			return _private.requestedPercentage;
		}
	};

	//------------------------------------------------
	// Get whether controls are visible as a boolean
	//------------------------------------------------
	_public.getControlsShowing = function() {
		return _private.controlsShowing;
	};

	//------------------------------------------------
	// Shows pause button
	//------------------------------------------------
	_public.showPauseButton = function() {
		_private.videoPlaying = true;
		var classes = _private.SELECTOR.querySelector(".play-button").className;
		if (classes.search("active") < 0) {
			classes = classes + " active";
			_private.SELECTOR.querySelector(".play-button").className = classes;
		}
	};

	//------------------------------------------------
	// Shows play button
	//------------------------------------------------
	_public.showPlayButton = function() {
		_private.onVideoPaused();
		var classes = _private.SELECTOR.querySelector(".play-button").className;
		if (classes.search("active") >= 0) {
			classes = classes.replace(/active| active/g, "");
			_private.SELECTOR.querySelector(".play-button").className = classes;
		}
	};

	//------------------------------------------------
	// Force controls to show
	//------------------------------------------------
	_public.show = function() {
		_private.show();
	};

	//------------------------------------------------
	// Force controls to hide
	//------------------------------------------------
	_public.hide = function() {
		_private.hide();
	};

	//------------------------------------------------
	// Start controls hide timer
	//------------------------------------------------
	_public.startAutoHiding = function() {
		_private.startHideTimer();
	};

	//------------------------------------------------
	// Hides the large play button. This means users
	// can click directly on the YouTube video
	//------------------------------------------------
	_public.hideLargePlayButton = function() {
		if (_private.SELECTOR.querySelector(".video-overlay")) {
			_private.SELECTOR.querySelector(".video-overlay").style.display = "none";
		}
	};

	//------------------------------------------------
	// Shows large play button
	//------------------------------------------------
	_public.showLargePlayButton = function() {
		if (_private.SELECTOR.querySelector(".video-overlay")) {
			_private.SELECTOR.querySelector(".video-overlay").style.display = "block";
		}
	};

	var _private = {
		//------------------------------------------------
		// Variables
		//------------------------------------------------
		SELECTOR: null,
		progressBar: null,
		options: {
			fade: false,
			slide: false,
			delay: 1000,
			duration: 300,
			mobileMode: false
		},
		height: 0,
		controlsShowing: true,
		lastMouseY: 0,
		videoPlaying: false,
		listeningForMouseMove: false,
		jQueryMode: false,
		requestedPercentage: 0,
		handleDragging: false,

		//------------------------------------------------
		// Create buttons
		//------------------------------------------------
		init: function() {
			this.SELECTOR = GACO.Utils.getDOMElement(selector);
			this.jQueryMode = (typeof jQuery !== "undefined");
			if (options) {
				for (var i in options) {
					this.options[i] = options[i];
				}
			}
			this.height = this.SELECTOR.querySelector(".controls").clientHeight;
			if (this.SELECTOR.querySelectorAll(".handle").length) {
				this.SELECTOR.querySelector(".handle").setAttribute("draggable", "false");
			}
			if (this.SELECTOR.querySelectorAll(".progress-bar").length) {
				this.createProgressBar();
			}
			if (!this.options.mobileMode) {
				this.createListeners();
			} else {
				this.createMobileListeners();
			}
		},

		//------------------------------------------------
		// Init progress bar
		//------------------------------------------------
		createProgressBar: function() {
			this.progressBar = new GACO.ProgressBar(this.SELECTOR.querySelector(".progress-bar"));
		},

		//------------------------------------------------
		// Listen for interaction
		//------------------------------------------------
		createListeners: function() {
			this.SELECTOR.querySelector(".play-button").addEventListener("click", function(e) {_private.onPlayButtonClicked(e);});
			if (this.SELECTOR.querySelector(".handle")) {
				this.SELECTOR.querySelector(".handle").addEventListener("mousedown", function(e) {_private.onHandleMouseDown(e);});
				this.SELECTOR.querySelector(".handle").addEventListener("click", function(e) {_private.onHandleClicked(e);});
			}
			if (this.SELECTOR.querySelector(".video-overlay")) {
				this.SELECTOR.querySelector(".video-overlay").addEventListener("click", function(e) {_private.onPlayButtonClicked(e);});
			}
			if (this.SELECTOR.querySelector(".mute-button")) {
				this.SELECTOR.querySelector(".mute-button").addEventListener("click", function(e) {_private.onMuteButtonClicked(e);});
			}
			if (this.SELECTOR.querySelector(".full-screen-button")) {
				this.SELECTOR.querySelector(".full-screen-button").addEventListener("click", function(e) {_private.onFullScreenButtonClicked(e);});
			}
			if (this.options.slide || this.options.fade) {
				this.SELECTOR.querySelector(".controls").addEventListener("mouseleave", function(e) {_private.onMouseLeave(e);});
			}
			if (this.progressBar) this.progressBar.addListener(this.progressBar.CLICKED, function(e) {_private.onProgressBarClicked(e);});
		},

		//------------------------------------------------
		// Hide placeholder on click
		//------------------------------------------------
		createMobileListeners: function(e) {
			if (this.SELECTOR.querySelector(".video-overlay")) {
				this.SELECTOR.querySelector(".video-overlay").addEventListener("click", function(e) {_private.onMobileOverlayClicked(e);});
			}
		},

		//------------------------------------------------
		// Auto-hide controls if requested
		//------------------------------------------------
		startHideTimer: function() {
			if (this.options.fade || this.options.slide) {
				if (!this.listeningForMouseMove) {
					this.SELECTOR.addEventListener("mousemove", _private.onMouseMoved);
					this.listeningForMouseMove = true;
				}
				clearTimeout(this.controlsTimer);
				this.controlsTimer = setTimeout(function(e) {_private.onControlsTimerTicked(e);}, this.options.delay);
			}
		},

		//------------------------------------------------
		// Hide controls
		//------------------------------------------------
		hide: function() {
			this.controlsShowing = false;
			if (this.jQueryMode) {
				if (this.options.fade) {
					$(this.SELECTOR).find(".controls").stop().fadeOut(300);
				} else if (this.options.slide) {
					$(this.SELECTOR).find(".controls").stop().animate({bottom: -this.height}, this.options.duration);
				}
			} else {
				if (this.options.fade) {
					GACO.Animation.start(this.SELECTOR.querySelector(".controls"), {opacity: 0}, {duration: this.options.duration});
				} else {
					GACO.Animation.start(this.SELECTOR.querySelector(".controls"), {bottom: -this.height}, {duration: this.options.duration});
				}
			}
		},

		//------------------------------------------------
		// Show controls
		//------------------------------------------------
		show: function() {
			this.controlsShowing = true;
			if (this.jQueryMode) {
				if (this.options.fade) {
					$(this.SELECTOR).find(".controls").stop().fadeIn(300);
				} else if (this.options.slide) {
					$(this.SELECTOR).find(".controls").stop().animate({bottom: 0}, this.options.duration);
				} else {
					$(this.SELECTOR).find(".controls").stop().show();
				}
			} else {
				if (this.options.fade) {
					GACO.Animation.start(this.SELECTOR.querySelector(".controls"), {opacity: 1}, {duration: this.options.duration});
				} else if (this.options.slide) {
					GACO.Animation.start(this.SELECTOR.querySelector(".controls"), {bottom: 0}, {duration: this.options.duration});
				} else {
					this.SELECTOR.querySelector(".controls").style.display = "block";
				}
			}
		},

		//------------------------------------------------
		// Keep checking mouse position to hide controls
		//------------------------------------------------
		restartTimer: function() {
			clearTimeout(this.controlsTimer);
			this.controlsTimer = setTimeout(function(e) {_private.onControlsTimerTicked(e);}, this.options.delay);
		},

		//------------------------------------------------
		// Start dragging handle
		//------------------------------------------------
		onHandleMouseDown: function(e) {
			_private.handleDragging = true;
			document.addEventListener("mousemove", _private.onHandleDragged);
			document.addEventListener("mouseup", _private.onHandleReleased);
			_public.trigger(_public.HANDLE_MOUSE_DOWN);
			e.stopPropagation();
			e.preventDefault();
		},

		//------------------------------------------------
		// Move play position
		//------------------------------------------------
		onHandleDragged: function(e) {
			var xPos = e.pageX - GACO.Utils.getOffset(_private.SELECTOR.querySelector(".hitarea")).left;
			var progressBarWidth = _private.SELECTOR.querySelector(".progress-bar").offsetWidth;
			var newPosition = xPos / progressBarWidth;
			if (newPosition < 0) newPosition = 0;
			var hitAreaWidthPercent = _private.SELECTOR.querySelector(".hitarea").offsetWidth / progressBarWidth;
			if (newPosition > hitAreaWidthPercent) newPosition = hitAreaWidthPercent;
			_private.requestedPercentage = newPosition;
			if (_private.progressBar) {
				_private.progressBar.setProgress(newPosition);
			}
			_private.SELECTOR.querySelector(".handle").style.left = (100 * newPosition) + "%";
			_public.trigger(_public.HANDLE_DRAGGED);
			e.stopPropagation();
			e.preventDefault();
		},

		//------------------------------------------------
		// Stop listening for mouse movements
		//------------------------------------------------
		onHandleReleased: function(e) {
			_private.handleDragging = false;
			document.removeEventListener("mousemove", _private.onHandleDragged);
			document.removeEventListener("mouseup", _private.onHandleReleased);
			_public.trigger(_public.HANDLE_RELEASED);
			e.stopPropagation();
			e.preventDefault();
		},

		//------------------------------------------------
		// Prevent default action
		//------------------------------------------------
		onHandleClicked: function(e) {
			e.stopPropagation();
			e.preventDefault();
		},

		//------------------------------------------------
		// Reset controls hide timer
		//------------------------------------------------
		onMouseMoved: function(e) {
			if (_private.hideTimer) {
				clearTimeout(_private.hideTimer)
				_private.hideTimer = null;
			}
			var newY = e.pageY - GACO.Utils.getOffset(_private.SELECTOR).top;
			var playerHeight = _private.SELECTOR.offsetHeight;
			if (newY !== _private.lastMouseY && !_private.controlsShowing && newY < playerHeight && newY > 0) {
				if (!_private.controlsShowing && newY < (_private.SELECTOR.offsetHeight)) {
					_private.show();
				}
			}
			_private.lastMouseY = newY;
			_private.restartTimer();
		},

		//------------------------------------------------
		// Change co-ords to something outside the container
		//------------------------------------------------
		onMouseLeave: function(e) {
			var newY = e.pageY - GACO.Utils.getOffset(this.SELECTOR).top;
			if (this.videoPlaying) {
				if (this.controlsShowing && (newY > (this.SELECTOR.offsetHeight - this.SELECTOR.querySelector(".controls").offsetHeight))) {
					this.lastMouseY = newY;
					clearTimeout(this.controlsTimer);
					this.hideTimer = setTimeout(function() {_private.hide();}, this.options.delay);
				}
			}
		},

		//------------------------------------------------
		// Switch states and signal click
		//------------------------------------------------
		onPlayButtonClicked: function(e) {
			var playButton = this.SELECTOR.querySelector(".play-button");
			var largePlayButton = this.SELECTOR.querySelector(".video-overlay");
			var playButtonClasses = playButton.className;
			var largePlayButtonClasses = "";
			if (largePlayButton) largePlayButtonClasses = largePlayButton.className;
			if (playButtonClasses.search("active") >= 0) {
				playButtonClasses = playButtonClasses.replace(/ active|active/g, "");
				if (largePlayButton) largePlayButtonClasses = largePlayButtonClasses.replace(/ active|active/g, "");
				clearTimeout(this.controlsTimer);
				this.videoPlaying = false;
				this.onVideoPaused();
				this.show();
				_public.trigger(_public.PAUSE_CLICKED);
			} else {
				playButtonClasses += " active";
				largePlayButtonClasses += " active";
				if (this.options.fade || this.options.slide) {
					this.startHideTimer();
				}
				this.videoPlaying = true;
				_public.trigger(_public.PLAY_CLICKED);
			}
			this.SELECTOR.querySelector(".play-button").className = playButtonClasses;
			this.show();
			if (largePlayButton) this.SELECTOR.querySelector(".video-overlay").className = largePlayButtonClasses;
			e.stopPropagation();
			e.preventDefault();
		},

		//------------------------------------------------
		// Switch states and signal click
		//------------------------------------------------
		onMuteButtonClicked: function(e) {
			var classes = e.currentTarget.className;
			if (classes.search("active") >= 0) {
				classes.replace(/ active|active/g, "");
				_public.trigger(_public.UNMUTE_CLICKED);
			} else {
				classes += " active";
				_public.trigger(_public.MUTE_CLICKED);
			}
			e.currentTarget.className = classes;
			e.stopPropagation();
			e.preventDefault();
		},

		//------------------------------------------------
		// Switch states and signal click
		//------------------------------------------------
		onFullScreenButtonClicked: function(e) {
			var classes = e.currentTarget.className;
			if (classes.search("active") >= 0) {
				classes.replace(/ active|active/g, "");
				_public.trigger(_public.EXIT_FULL_SCREEN_CLICKED);
			} else {
				classes += " active";
				_public.trigger(_public.ENTER_FULL_SCREEN_CLICKED);
			}
			e.currentTarget.className = classes;
			e.stopPropagation();
			e.preventDefault();
		},

		//------------------------------------------------
		// Signal a click has occurred
		//------------------------------------------------
		onProgressBarClicked: function(e) {
			_public.trigger(_public.PROGRESS_CLICKED);
		},

		//------------------------------------------------
		// Signal a click has occurred
		//------------------------------------------------
		onControlsTimerTicked: function(e) {
			if (!this.handleDragging && this.videoPlaying && this.controlsShowing && this.lastMouseY < (this.SELECTOR.offsetHeight - this.SELECTOR.querySelector(".controls").offsetHeight)) {
				clearTimeout(this.controlsTimer);
				this.hide();
			}
		},

		//------------------------------------------------
		// Stop hiding controls if in progress
		//------------------------------------------------
		onVideoPaused: function() {
			this.videoPlaying = false;
			if (this.controlsTimer) clearTimeout(this.controlsTimer);
			this.listeningForMouseMove = false;
			this.SELECTOR.removeEventListener("mousemove", _private.onMouseMoved);
		},

		//------------------------------------------------
		// Hide overlay so user can play video on mobiles
		//------------------------------------------------
		onMobileOverlayClicked: function(e) {
			this.SELECTOR.style.display = "none";
			_public.trigger(_public.OVERLAY_HIDDEN);
			e.stopPropagation();
			e.preventDefault();
		}
	};

	_private.init();
	return _public;
};