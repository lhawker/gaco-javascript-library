////////////////////////////////////////////////////////////////////////////////
// GACO Library
// Basic YouTube Video Player using the iFrame API
// Can optionally use swfobject for cleanup (not needed)
// - data-video should be on the main selector with the video URL
// - If full screen functionality is required, the browser must support
// the fullscreen API.
// - Selector should contain '.player' for the video
// - options: {
//			controls: true,	- Will show default YouTube controls if true
//			autoPlay: false,	- Whether video should immediately start playing
//			preventInit: false, - Will prevent the YouTube API from loading until playNewVideo is called,
//			loop: false, - Loops video if required,
//			mute: false - Prevent any sound playing,
//			annotations: false - Shows YouTube annotations when true
//			mobileMode: false - Does stuff like forcing default controls for mobiles/tablets
//	 }
////////////////////////////////////////////////////////////////////////////////
GACO.Utils.createNamespace("GACO.video");

GACO.video.BasicYouTubePlayer = function(selector, options) {

	"use strict";
	
	var _public = new GACO.EventDispatcher();

	//------------------------------------------------
	// Events
	//------------------------------------------------
	_public.PLAYER_READY = "PlayerReady";
	_public.VIDEO_COMPLETE = "VideoComplete";
	_public.VIDEO_PLAYING = "VideoPlaying";
	_public.VIDEO_PAUSED = "VideoPaused";
	_public.PLAY_PROGRESS = "PlayProgress";
	_public.LOAD_PROGRESS = "LoadProgress";

	//------------------------------------------------
	// Returns true if this video has started playing yet
	//------------------------------------------------
	_public.getHasPlayTriggered = function() {
		return _private.firstPlayHasHappened;
	};

	//------------------------------------------------
	// Play a new video (stop any currently playing)
	//------------------------------------------------
	_public.playNewVideo = function(autoPlay, videoURL) {
		_private.getVideoID(videoURL);
		_private.playRequested = false;
		if (!_private.player || !_private.player.playVideo) {
			_private.autoPlay = autoPlay;
			if (!_private.apiLoading) {
				_private.apiLoading = true;
				_private.loadAPI();
				_private.createListeners();
			} else {
				_private.autoPlayTimer = setTimeout(function() {_private.onAutoPlayTimerTicked();}, 20);
			}
		} else {
			if (_private.touchDevice && !_private.firstPlayHasHappened) {
				_private.player.cueVideoById(_private.videoID);
			} else if (!_private.touchDevice || (_private.touchDevice && _private.firstPlayHasHappened)) {
				_private.player.loadVideoById(_private.videoID);
			}
		}
	};

	//------------------------------------------------
	// Rebuild the YouTube player
	//------------------------------------------------
	_public.rebuild = function(autoPlay, videoURL) {
		_private.stopLoadTimer();
		_private.stopPlayTimer();
		_private.videoURL = videoURL;
		_private.getVideoID(videoURL);
		_private.autoPlay = autoPlay;
		_private.player = null;
		_private.apiLoading = true;
		_private.firstPlayHasHappened = false;
		if (_private.touchDevice && _private.DOMElement.querySelectorAll(".large-play-button").length) {
			_private.DOMElement.querySelector(".large-play-button").style.display = "none";
		}
		if (typeof YT === "undefined") {
			_private.loadAPI();
		} else {
			_private.createPlayer();
		}
	};

	//------------------------------------------------
	// Kill this player
	//------------------------------------------------
	_public.remove = function() {
		if (typeof swfobject !== "undefined") swfobject.removeSWF("youtube-video");
		window.onPlayerStateChange = function() {};
		if (_private.player) if (_private.player.stopVideo) _private.player.stopVideo();
		_private.DOMElement.querySelector(".player").innerHTML = "";
	};

	//------------------------------------------------
	// Play
	//------------------------------------------------
	_public.playVideo = function() {
		if (_private.player && _private.player.playVideo) {
			if (_private.touchDevice) {
				if (_private.firstPlayHasHappened) {
					if (_private.player) {
						_private.player.playVideo();
					}
				}
			} else {
				if (_private.player) {
					_private.player.playVideo();
				}
			}
		} else {
			_private.playRequested = true;
		}
	};

	//------------------------------------------------
	// Pause
	//------------------------------------------------
	_public.pauseVideo = function() {
		if (_private.player) {
			if (_private.player.pauseVideo) {
				_private.player.pauseVideo();
			}
		}
		_private.stopLoadTimer();
	};

	//------------------------------------------------
	// Mute
	//------------------------------------------------
	_public.mute = function() {
		if (!_private.muted) {
			_private.muted = true;
			if (_private.player)_private.player.mute();
		}
	};

	//------------------------------------------------
	// Unmute
	//------------------------------------------------
	_public.unMute = function() {
		if (_private.muted) {
			_private.muted = false;
			if (_private.player)_private.player.unMute();
		}
	};

	//------------------------------------------------
	// Seek video to position
	//------------------------------------------------
	_public.seekTo = function(time) {
		_private.seekingToTime = Math.floor(time);
		if (_private.player) if (_private.player.seekTo) _private.player.seekTo(time);
		if (!_private.videoPlaying) {
			this.pauseVideo();
		}
	};

	//------------------------------------------------
	// Returns current play position
	//------------------------------------------------
	_public.getCurrentTime = function() {
		return _private.player.getCurrentTime();
	};

	//------------------------------------------------
	// Returns current progress percentage
	//------------------------------------------------
	_public.getPlayProgress = function() {
		return Math.floor(_private.playProgress * 100) / 100;
	};

	//------------------------------------------------
	// Returns percentage of current video loaded
	//------------------------------------------------
	_public.getVideoLoadedFraction = function() {
		return _private.player.getVideoLoadedFraction();
	};

	//------------------------------------------------
	// Returns duration of current video
	//------------------------------------------------
	_public.getDuration = function() {
		return _private.player.getDuration();
	};

	//------------------------------------------------
	// Returns string with duration in 0:00:00 format
	//------------------------------------------------
	_public.getDurationText = function() {
		return _private.durationText;
	};

	//------------------------------------------------
	// Returns string with current time in 0:00:00 format
	//------------------------------------------------
	_public.getCurrentTimeText = function() {
		return _private.currentTimeText;
	};

	//------------------------------------------------
	// Returns whether player can be used yet
	//------------------------------------------------
	_public.isReady = function() {
		return _private.playerReadyState;
	};

	//------------------------------------------------
	// Returns if video is currently playing
	//------------------------------------------------
	_public.isPlaying = function() {
		return _private.videoPlaying;
	};

	//------------------------------------------------
	// Enters full screen mode
	//------------------------------------------------
	_public.enterFullScreen = function() {
		_private.fullScreen = true;
		var container = _private.DOMElement;
		if (container.requestFullScreen) {
			container.requestFullscreen();
		} else if (container.webkitRequestFullScreen) {
			container.webkitRequestFullScreen();
		} else if (container.mozRequestFullScreen) {
			container.mozRequestFullScreen();
		} else if (container.msRequestFullScreen) {
			container.msRequestFullScreen();
		}
	};

	//------------------------------------------------
	// Exits full screen mode
	//------------------------------------------------
	_public.exitFullScreen = function() {
		_private.fullScreen = false;
		if (document.exitFullscreen) {
			document.exitFullScreen();
		} else if (document.webkitCancelFullScreen) {
			document.webkitCancelFullScreen();
		} else if (document.mozCancelFullScreen) {
			document.mozCancelFullScreen();
		} else if (document.msCancelFullScreen) {
			document.msCancelFullScreen();
		}
	};

	var _private = {

		//------------------------------------------------
		// Variables
		//------------------------------------------------
		DOMElement: null,
		API_URL: "http://www.youtube.com/iframe_api",
		apiLoading: false,
		options: {
			controls: true,
			autoPlay: false,
			preventInit: false,
			loop: false,
			mute: false,
			mobileMode: false
		},
		videoID: "",
		player: null,
		playProgress: 0,
		videoPlaying: false,
		muted: false,
		currentTime: 0,
		currentMinutes: 0,
		currentSeconds: 0,
		currentTimeText: "00.00",
		duration: 0,
		durationMinutes: 0,
		durationSeconds: 0,
		durationText: "00.00",
		playTimer: null,
		loadProgress: 0,
		splashOverride: false,
		seekingToTime: 0,
		fullScreen: false,
		playerReadyState: false,
		playCheckTimer: null,
		playRequested: false,
		firstPlayHasHappened: false,

		//------------------------------------------------
		// Init player
		//------------------------------------------------
		init: function() {
			this.DOMElement = GACO.Utils.getDOMElement(selector);
			if (options) {
				for (var i in options) {
					this.options[i] = options[i];
				}
			}
			if (this.options.mobileMode) {
				this.options.controls = true;
				this.options.autoPlay = false;
			}
			var videoURL = this.DOMElement.getAttribute("data-video");
			if (videoURL) this.getVideoID(videoURL);
			if (!this.options.preventInit && videoURL) {
				this.loadAPI();
				this.createListeners();
			}
		},

		//------------------------------------------------
		// Get ID from video URL
		//------------------------------------------------
		getVideoID: function(url) {
			var startIndex = 0, endIndex = 0;
			if (url.search("youtube.com") > -1) {
				startIndex = parseInt(url.search("v=")) + 2;
			} else {
				startIndex = parseInt(url.search("youtu.be/")) + 9;
			}
			if (url.search("&") > -1) {
				endIndex = url.search("&");
			} else if (url.search("#") > -1) {
				endIndex = url.search("#");
			} else {
				endIndex = url.length;
			}
			this.videoID = url.substr(startIndex, (endIndex - startIndex));
		},

		//------------------------------------------------
		// Load YouTube API
		//------------------------------------------------
		loadAPI: function() {
			if (!document.documentElement.querySelectorAll("#youtube-api").length) {
				GACO.Utils.ajax({
					url: this.API_URL,
					dataType: "script",
					id: "youtube-api",
					success: function(data) {
						_private.apiLoading = true;
						_private.createPlayer();
					},
					error: function(request, status, error) {
						if (window.console) console.log("YouTube API Load Error: Status = " + status + ", error = " + error);
					}
				});
			} else {
				_private.apiLoading = true;
				_private.createPlayer();
			}
		},

		//------------------------------------------------
		// Listen for full screen exit
		//------------------------------------------------
		createListeners: function() {
			var fullScreenEventNames = ["fullscreenchange", "msfullscreenchange", "webkitfullscreenchange", "mozfullscreenchange"]
			for (var eventName in fullScreenEventNames) {
				
				if(document.addEventListener){
					document.documentElement.addEventListener(eventName, function(e) {_private.onFullScreenChange(e);});
				} else {
					document.documentElement.attachEvent(eventName, function(e) {_private.onFullScreenChange(e);});
				}
			}
		},

		//------------------------------------------------
		// Wait for player object to exist before creation
		//------------------------------------------------
		createPlayer: function() {
			if (typeof YT !== "undefined") {
				if (YT.Player) {
					this.player = new YT.Player(this.DOMElement.querySelector(".player"), {
						width: "100%",
						height: "100%",
						videoId: this.videoID,
						playerVars: {
							controls: (this.options.controls) ? 1 : 0,
							modestBranding: 1,
							showinfo: 0,
							rel: 0,
							disablekb: 0,
							enablejsapi: 1,
							wmode: "opaque",
							autoplay: (this.autoPlay) ? 1 : 0,
							loop: (this.options.loop) ? 1 : 0,
							iv_load_policy: (this.options.annotations) ? 1 : 3
						},
						events: {
							"onReady": function() {_private.onPlayerReady();},
							"onStateChange": function(e) {_private.onPlayerStateChange(e);},
							"onError": function(e) {_private.onVideoError(e);}
						}
					});
				} else {
					// Wait for player to exist
					if (this.youtubeCheckTimer) {
						clearTimeout(this.youtubeCheckTimer);
						this.youtubeCheckTimer = null;
					}
					this.youtubeCheckTimer = setTimeout(function() {_private.createPlayer();}, 300);
				}
			} else {
				// Wait for player to exist
				if (this.youtubeCheckTimer) {
					clearTimeout(this.youtubeCheckTimer);
					this.youtubeCheckTimer = null;
				}
				this.youtubeCheckTimer = setTimeout(function() {_private.createPlayer();}, 300);
			}
		},

		//------------------------------------------------
		// Loop that runs during playback
		//------------------------------------------------
		startPlayTimer: function() {
			clearInterval(this.playTimer);
			this.playTimer = setInterval(function(e) {_private.onPlayProgress();}, 20);
		},

		//------------------------------------------------
		// Stop look that runs during playback
		//------------------------------------------------
		stopPlayTimer: function() {
			clearInterval(this.playTimer);
		},

		//------------------------------------------------
		// Loop that runs during load
		//------------------------------------------------
		startLoadTimer: function() {
			if (this.loadTimer) clearInterval(this.loadTimer);
			this.loadTimer = setInterval(function() {_private.onLoadProgress();}, 20);
		},

		//------------------------------------------------
		// Stop updating load progress
		//------------------------------------------------
		stopLoadTimer: function() {
			if (this.loadTimer) clearInterval(this.loadTimer);
		},

		//------------------------------------------------
		// Put together current time string
		//------------------------------------------------
		getCurrentTime: function() {
			this.currentTime = Math.floor(this.player.getCurrentTime());
			// Keep display up to date when seek in progress
			if ((this.seekingToTime > 0) && (this.currentTime !== this.seekingToTime)) {
				this.currentTime = this.seekingToTime;
			} else {
				this.seekingToTime = 0;
			}
			this.currentMinutes = Math.floor(this.currentTime/60);
			this.currentSeconds = (this.currentTime%60);
			if (this.currentMinutes > 0) {
				this.currentTimeText = this.currentMinutes + ".";
			} else {
				this.currentTimeText = "0.";
			}
			if (this.currentSeconds > 0) {
				if (this.currentSeconds < 10) {
					this.currentTimeText += "0" + this.currentSeconds;
				} else {
					this.currentTimeText += this.currentSeconds;
				}
			} else {
				this.currentTimeText += "00";
			}
		},

		//------------------------------------------------
		// Put together duration string
		//------------------------------------------------
		getVideoDuration: function() {
			this.duration = Math.floor(this.player.getDuration());
			this.durationMinutes = Math.floor(this.duration/60);
			this.durationSeconds = Math.floor(this.duration%60);
			if (this.durationMinutes > 0) {
				this.durationText = this.durationMinutes + ".";
			} else {
				this.durationText = "0.";
			}
			if (this.durationSeconds > 0) {
				if (this.durationSeconds < 10) {
					this.durationText += "0" + this.durationSeconds; 
				} else {
					this.durationText += this.durationSeconds;
				}
			} else {
				this.durationText += "00";
			}
		},

		//------------------------------------------------
		// Update time and progress displayed
		//------------------------------------------------
		onPlayProgress: function() {
			// Work out current time as a string
			this.getCurrentTime();
			// Work out duration as a string
			this.getVideoDuration();
			var newProgress = 0;
			// Keep display up to date when seek in progress
			if (this.seekingToTime > 0) {
				newProgress = this.seekingToTime / this.player.getDuration();
			} else {
				newProgress = this.currentTime / this.player.getDuration();
			}
			if (newProgress !== this.playProgress) {
				this.playProgress = newProgress;
				_public.trigger(_public.PLAY_PROGRESS);
			}
		},

		//------------------------------------------------
		// Update current load percentage
		//------------------------------------------------
		onLoadProgress: function() {
			var newProgress = this.player.getVideoLoadedFraction();
			if (newProgress !== this.loadProgress) {
				_public.trigger(_public.LOAD_PROGRESS);
				this.loadProgress = newProgress;
			}
		},

		//------------------------------------------------
		// Player is, in theory, ready
		//------------------------------------------------
		onPlayerReady: function(e) {
			if (this.player.playVideo) {
				this.onPlayerReadyToPlay();
			} else {
				// Keep checking until play function is ready
				clearTimeout(this.playCheckTimer);
				this.playCheckTimer = setTimeout(function() {_private.onPlayerReady();}, 20);
			}
		},

		//------------------------------------------------
		// Player is now PROPERLY ready to go
		//------------------------------------------------
		onPlayerReadyToPlay: function() {
			this.playerReadyState = true;
			this.startLoadTimer();
			if (this.playRequested && !this.pauseRequested) {
				this.playRequested = false;
				_public.playVideo();
			}
			_public.trigger(_public.PLAYER_READY);
		},

		//------------------------------------------------
		// Handle state changes
		//------------------------------------------------
		onPlayerStateChange: function(e) {
			if (e.data === YT.PlayerState.ENDED) {
				this.playProgress = 1;
				this.videoPlaying = false;
				this.stopPlayTimer();
				this.playProgress = 1;
				_public.trigger(_public.PLAY_PROGRESS);
				_public.trigger(_public.VIDEO_COMPLETE);
			} else if (e.data === YT.PlayerState.PLAYING) {
				this.videoPlaying = true;
				this.firstPlayHasHappened = true;
				this.startPlayTimer();
				if (this.options.controls) this.startPlayTimer();
				if (this.options.mute) _public.mute();
				_public.trigger(_public.VIDEO_PLAYING);
			} else if (e.data === YT.PlayerState.PAUSED) {
				this.stopPlayTimer();
				this.videoPlaying = false;
				_public.trigger(_public.VIDEO_PAUSED);
			}
		},

		//------------------------------------------------
		// Error with video playback
		//------------------------------------------------
		onVideoError: function(e) {
			if (window.console) console.dir(e);
		},

		//------------------------------------------------
		// Reset state if full screen exited
		//------------------------------------------------
		onFullScreenChange: function(e) {
			var fullScreenElement  = document.fullscreenElement  || document.msFullscreenElement || document.webkitFullscreenElement || document.webkitCurrentFullScreenElement || document.mozFullscreenElement;
			if (!fullScreenElement) {
				_private.fullScreen = false;
				_private.DOMElement.removeAttr("style");
			} else {
				_private.fullScreen = true;
				_private.DOMElement.style.width = "100%";
				_private.DOMElement.style.height = "100%";
			}
		}
	};

	_private.init();
	return _public;
};