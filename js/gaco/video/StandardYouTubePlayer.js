////////////////////////////////////////////////////////////////////////////////
// GACO Library
// Basic YouTube Video Player using the iFrame API
// - Selector should contain '.player' for the video, '.controls-container' and
// and '.controls' for the controls
// - Selector should have the data attribute 'data-video' with a YouTube URL
// for the first video
// - options are the standard BasicYouTubePlayer options (see BasicYouTubePlayer.js)
// - controlsOptions are the standard controls options (see: Controls.js)
// - If full screen functionality is required, the browser must support
// the fullscreen API.
////////////////////////////////////////////////////////////////////////////////
GACO.Utils.createNamespace("GACO.video");

GACO.video.StandardYouTubePlayer = function(selector, options, controlsOptions) {

	"use strict";
	
	var _public = new GACO.video.BasicYouTubePlayer(selector, options);

	var _private = {
		//------------------------------------------------
		// Variables
		//------------------------------------------------
		controls: null,
		autoPlay: false,
		options: {},
		controlsOptions: {},
		videoWasPlaying: false,

		//------------------------------------------------
		// Init
		//------------------------------------------------
		init: function() {
			this.SELECTOR = GACO.Utils.getDOMElement(selector);
			this.options = options;
			this.controlsOptions = controlsOptions;
			this.createControls();
			this.createListeners();
		},

		//------------------------------------------------
		// Creates basic controls
		//------------------------------------------------
		createControls: function() {
			this.controls = new GACO.video.Controls(".controls-container", this.controlsOptions);
		},

		//------------------------------------------------
		// Listen for user interaction
		//------------------------------------------------
		createListeners: function() {
			// Handle events from the controls
			this.controls.addListener(this.controls.PLAY_CLICKED, function(e) {_private.onPlayClicked(e);});
			this.controls.addListener(this.controls.PAUSE_CLICKED, function(e) {_private.onPauseClicked(e);});
			this.controls.addListener(this.controls.MUTE_CLICKED, function(e) {_private.onMuteClicked(e);});
			this.controls.addListener(this.controls.UNMUTE_CLICKED, function(e) {_private.onUnMuteClicked(e);});
			this.controls.addListener(this.controls.PROGRESS_CLICKED, function(e) {_private.onProgressClicked(e);});
			this.controls.addListener(this.controls.ENTER_FULL_SCREEN_CLICKED, function(e) {_private.onEnterFullScreenClicked(e);});
			this.controls.addListener(this.controls.EXIT_FULL_SCREEN_CLICKED, function(e) {_private.onExitFullScreenClicked(e);});
			this.controls.addListener(this.controls.HANDLE_MOUSE_DOWN, function(e) {_private.onHandleMouseDown(e);});
			this.controls.addListener(this.controls.HANDLE_DRAGGED, function(e) {_private.onHandleDragged(e);});
			this.controls.addListener(this.controls.HANDLE_RELEASED, function(e) {_private.onHandleReleased(e);});
			// Handle events from the player
			_public.addListener(_public.VIDEO_COMPLETE, function(e) {_private.onVideoComplete(e);});
			_public.addListener(_public.PLAY_PROGRESS, function(e) {_private.onPlayProgress(e);});
			_public.addListener(_public.LOAD_PROGRESS, function(e) {_private.onLoadProgress(e);});
			_public.addListener(_public.VIDEO_PLAYING, function(e) {_private.onVideoPlaying(e);});
			_public.addListener(_public.VIDEO_PAUSED, function(e) {_private.onVideoPaused(e);});
		},

		//------------------------------------------------
		// Start video playing
		//------------------------------------------------
		onPlayClicked: function(e) {
			_public.playVideo();
		},

		//------------------------------------------------
		// Stop video playing
		//------------------------------------------------
		onPauseClicked: function(e) {
			_public.pauseVideo();
		},

		//------------------------------------------------
		// Mute sound in the video
		//------------------------------------------------
		onMuteClicked: function(e) {
			_public.mute();
		},

		//------------------------------------------------
		// Un-mute sound in the video
		//------------------------------------------------
		onUnMuteClicked: function(e) {
			_public.unMute();
		},

		//------------------------------------------------
		// Progress video to requested percentage
		//------------------------------------------------
		onProgressClicked: function(e) {
			var percentage = this.controls.getRequestedPercentage();
			_public.seekTo(percentage * _public.getDuration());
			this.controls.setProgress(percentage);
		},

		//------------------------------------------------
		// Make video full screen
		//------------------------------------------------
		onEnterFullScreenClicked: function(e) {
			_public.enterFullScreen();
		},

		//------------------------------------------------
		// Exit full screen mode
		//------------------------------------------------
		onExitFullScreenClicked: function(e) {
			_public.exitFullScreen();
		},

		//------------------------------------------------
		// Show controls start state
		//------------------------------------------------
		onVideoComplete: function(e) {
			this.controls.setProgress(1);
			this.controls.showPlayButton();
		},

		//------------------------------------------------
		// Update play position in controls
		//------------------------------------------------
		onPlayProgress: function(e) {
			var progress = _public.getPlayProgress();
			this.controls.setProgress(progress);
		},

		//------------------------------------------------
		// Update load progress in controls
		//------------------------------------------------
		onLoadProgress: function(e) {
			var progress = _public.getVideoLoadedFraction();
			this.controls.setLoaded(progress);
		},

		//------------------------------------------------
		// Video has been played
		//------------------------------------------------
		onVideoPlaying: function(e) {
			this.controls.showPauseButton();
		},

		//------------------------------------------------
		// Video has been paused
		//------------------------------------------------
		onVideoPaused: function(e) {
			this.controls.showPlayButton();
		},

		//------------------------------------------------
		// User has started dragging handle
		//------------------------------------------------
		onHandleMouseDown: function(e) {
			this.videoWasPlaying = this.player.isPlaying();
		},

		//------------------------------------------------
		// Do nothing for now
		//------------------------------------------------
		onHandleDragged: function(e) {

		},

		//------------------------------------------------
		// Start playing again if needed
		//------------------------------------------------
		onHandleReleased: function(e) {
			var percentage = this.controls.getRequestedPercentage();
			var duration = this.player.getDuration();
			this.player.pauseVideo();
			this.player.seekTo(percentage * duration);
			this.controls.setProgress(percentage);
			if (this.videoWasPlaying) {
				this.videoWasPlaying = false;
				this.player.playVideo();
			}
		}
	};

	_private.init();
	return _public;
};