////////////////////////////////////////////////////////////////////////////////
// GACO Library
// Progress bar
// Selector must contain '.progress', 'hitarea'
// Selector can optionally contain '.loaded'
////////////////////////////////////////////////////////////////////////////////
var GACO = GACO || {};

GACO.ProgressBar = function(selector) {

	"use strict";

	var _public = new GACO.EventDispatcher();

	//------------------------------------------------
	// Public variables
	//------------------------------------------------
	_public.CLICKED = "Clicked";

	//------------------------------------------------
	// Returns position user wants to scan to
	//------------------------------------------------
	_public.getRequestedPercentage = function() {
		return _private.requestedPercentage;
	};

	//------------------------------------------------
	// Update play progress bar
	//------------------------------------------------
	_public.setProgress = function(percentage) {
		if (percentage > 1) percentage = 1;
		var newWidth = Math.round(percentage * 100);
		if (newWidth > 0) {
			newWidth += "%";
		} else {
			newWidth = 0;
		}
		_private.SELECTOR.querySelector(".progress").style.width = newWidth;
		_private.progressPercentage = _private.requestedPercentage = percentage;
	};

	//------------------------------------------------
	// Update load progress bar
	//------------------------------------------------
	_public.setLoaded = function(percentage, adjustHitArea) {
		if (percentage > 1) percentage = 1;
		var newWidth = Math.round(percentage * 100) + "%";
		_private.SELECTOR.querySelector(".loaded").style.width = newWidth;
		if (adjustHitArea) {
			_private.SELECTOR.querySelector(".hitarea").style.width = newWidth;
		}
		_private.loadedPercentage = percentage;
	};

	var _private = {
		//------------------------------------------------
		// Variables
		//------------------------------------------------
		SELECTOR: null,
		progressPercentage: 0,
		loadedPercentage: 0,
		requestedPercentage: 0,

		//------------------------------------------------
		// Init
		//------------------------------------------------
		init: function() {
			this.SELECTOR = GACO.Utils.getDOMElement(selector);
			this.SELECTOR.querySelector(".progress").offsetWidth;
			this.SELECTOR.querySelector(".loaded").offsetWidth;
			this.createListeners();
		},

		//------------------------------------------------
		// Listen for interaction
		//------------------------------------------------
		createListeners: function() {
			this.SELECTOR.querySelector(".hitarea").addEventListener("click", function(e) {_private.onHitAreaClicked(e);});	
		},

		//------------------------------------------------
		// Store position and signal click
		//------------------------------------------------
		onHitAreaClicked: function(e) {
			var clickX = e.pageX - GACO.Utils.getOffset(e.currentTarget).left;
			this.requestedPercentage = clickX / this.SELECTOR.offsetWidth;
			_public.trigger(_public.CLICKED);
			e.stopPropagation();
			e.preventDefault();
		}
	};

	_private.init();
	return _public;
};