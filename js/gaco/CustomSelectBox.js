////////////////////////////////////////////////////////////////////////////////
// GACO Library
// Custon Styled Selectbox
////////////////////////////////////////////////////////////////////////////////
var GACO = GACO || {};

GACO.CustomSelectBox = function(selector, formElementSelector, label, hideParentForm) {
	var _public = new GACO.EventDispatcher();

	//------------------------------------------------
	// Returns selector for this dropdown
	//------------------------------------------------
	_public.getSelector = function() {
		return _private.SELECTOR;
	};

	//------------------------------------------------
	// Returns currently selected value
	//------------------------------------------------
	_public.getSelectedOption = function() {
		return _private.selectedOption;
	};

	//------------------------------------------------
	// Returns visible label for selected option
	//------------------------------------------------
	_public.getSelectedOptionLabel = function() {
		return _private.selectedOptionLabel;
	};

	//------------------------------------------------
	// Returns option ID
	//------------------------------------------------
	_public.getSelectedOptionID = function() {
		return _private.selectedOptionID;
	};

	//------------------------------------------------
	// Returns whether dropdown is open or not
	//------------------------------------------------
	_public.getOpenState = function() {
		return _private.open;
	};

	//------------------------------------------------
	// Close dropdown externally
	//------------------------------------------------
	_public.close = function() {
		_private.closeDropdown();
	};

	//------------------------------------------------
	// Open dropdown externally
	//------------------------------------------------
	_public.open = function() {
		_private.openDropdown();
	};

	//------------------------------------------------
	// Set the active dropdown value manually
	//------------------------------------------------
	_public.setActiveOptionID = function(id) {
		_private.selectedFormOption = _private.selectedOptionID = id;
		_private.selectedOptionLabel = _private.SELECTOR.find("ul a").eq(id).html();
		_private.selectedOption = _private.SELECTOR.find("ul a").eq(id).attr("href");
		_private.SELECTOR.find("ul a").removeClass("active");
		_private.SELECTOR.find("ul a").eq(id).addClass("active");
		_private.SELECTOR.find(".current").html(_private.selectedOptionLabel);
		_private.FORM_SELECTOR.find("option").removeAttr("selected");
		_private.FORM_SELECTOR.find("option").eq(id).attr("selected", "selected");
		_private.closeDropdown();
	}

	var _private = {

		//------------------------------------------------
		// Variables
		//------------------------------------------------
		SELECTOR: null,
		FORM_SELECTOR: null,
		open: false,
		selectedOption: "",
		selectedOptionID: 0,
		scrollbar: null,
		initialLabel: "",
		hideParentForm: false,
		selectedFormOption: -1,
		selectedOptionLabel: "",

		//------------------------------------------------
		// Init
		//------------------------------------------------
		init: function() {
			this.SELECTOR = $(selector);
			this.FORM_SELECTOR = $(formElementSelector);
			this.initialLabel = label;
			this.hideParentForm = hideParentForm;

			if (formElementSelector) {
				this.createDropdown();
			} else {
				this.SELECTOR.find(".global-dropdown").height(this.SELECTOR.find(".current").outerHeight());
				this.setActiveOption();
			}
			
			var maxHeight = this.SELECTOR.find(".content").css("max-height").replace("px", "");
			
			maxHeight = (maxHeight > 0) ? maxHeight : 264;
			
			if (this.SELECTOR.find(".scrollable").outerHeight() > maxHeight) {
				this.SELECTOR.find(".content").height("auto");
				/* HAWKS this.createScrollbar(); */
				//this.createScrollbar();
				this.SELECTOR.find(".content").height(0).hide();
			}
			
			this.createListeners();
			this.createScrollbar();

			if (this.hideParentForm) {
				this.FORM_SELECTOR.closest("form").hide();
			} else {
				this.FORM_SELECTOR.hide();
			}
		},

		//------------------------------------------------
		// Create fake dropdown
		//------------------------------------------------
		createDropdown: function() {

			var templateCode = $('#dropdown-template').html();
			var data = {}, options = [], i = 0, total = this.FORM_SELECTOR.find("option").length,
			label = "", optionSelected;
			// Check for 'selected' on the options in the select field first
			optionSelected = this.FORM_SELECTOR.find("option[selected='selected']");
			if (optionSelected.length) {
				this.selectedFormOption = this.FORM_SELECTOR.find("option").index(optionSelected);
			}
			data.defaultOption = this.initialLabel;
			// Gather data for each option
			for (; i < total; i++) {
				label = this.FORM_SELECTOR.find("option").eq(i).html();
				if (i === 0 && !this.initialLabel && this.selectedFormOption < 0) data.defaultOption = label;
				optionOdd = ((i+1) % 2 === 0) ? "even" : "odd"
				options.push({
					optionLabel: label.replace(/&amp;/g, "&"),
					optionValue: this.FORM_SELECTOR.find("option").eq(i).val(),
					optionClass: optionOdd
				});
			}
			// Default to 'selected' option if one exists in the original form field
			if (this.selectedFormOption > -1) {
				data.defaultOption = this.FORM_SELECTOR.find("option").eq(this.selectedFormOption).html();
			}
			// Create new dropdown from template
			if (options.length > 0) {
				var template = Handlebars.compile(templateCode);
				Handlebars.registerHelper("defaultOption", function() {
					return new Handlebars.SafeString(this.defaultOption);
				});
				data.options = options;
				var html = template(data);
				this.SELECTOR.html(html);
				this.SELECTOR.find(".global-dropdown").height(this.SELECTOR.find(".current").outerHeight());
				this.SELECTOR.find(".global-dropdown .current").attr("href", data.options[0].optionValue);
				this.setActiveOption();
			}
		},

		//------------------------------------------------
		// Mark an option in dropdown as active
		//------------------------------------------------
		setActiveOption: function() {
			var totalOptions = this.SELECTOR.find("ul li").length, i = 0;
			$('.scrollable ul li a').first().css({'display': 'none'});
			
			var defaultText = this.SELECTOR.find(".global-dropdown>a").html();
			var value;
			for (; i < totalOptions; i++) {
				value = this.SELECTOR.find("ul a").eq(i).html();
				if (value === defaultText) {
					this.SELECTOR.find("ul a").eq(i).addClass("active");
					this.selectedOption = this.SELECTOR.find("ul a").eq(i).attr("href");
					this.selectedOptionID = i;
					break;
				}
			}
		},

		//------------------------------------------------
		// Add working scrollbar
		//------------------------------------------------
		createScrollbar: function(){
			/* May think about adding the below plugin !HUM don't know? 
			http://jscrollpane.kelvinluck.com/fullpage_scroll.html */

			var y = this.SELECTOR.find(".scrollable").scrollTop(),
				parentHeight = this.SELECTOR.find(".scrollable").parent().innerHeight(), 
				contentHeight = this.SELECTOR.find(".scrollable ul").outerHeight(),
				totalHeight = contentHeight - parentHeight;

			if(y === 0) {
				this.SELECTOR.find(".content .scrollbarbottom").fadeIn();
			}

			if(y > 5) {
				this.SELECTOR.find(".content .scrollbartop").fadeIn();
			} else if(y <=0){
				this.SELECTOR.find(".content .scrollbartop").fadeOut();
			}

			if(y > 0) {
				if(y === totalHeight){
					this.SELECTOR.find(".content .scrollbarbottom").fadeOut();
				} else {
					this.SELECTOR.find(".content .scrollbarbottom").fadeIn();
				}
			}
		},

		//------------------------------------------------
		// Scroll dropdow content
		//------------------------------------------------
		scrollContent: function(e, direction){
			$(document).off("click");

			var y = this.SELECTOR.find(".scrollable").scrollTop(),
				scrollableSelector = this.SELECTOR.find(".scrollable");
			
			if(direction === "down") scrollableSelector.scrollTop(y + 20);
			if(direction ==="up") scrollableSelector.scrollTop(y - 20);	
		},

		//------------------------------------------------
		// Listen for mouse clicks
		//------------------------------------------------
		createListeners: function() {
			this.SELECTOR.find(".current").on("click", function(e) {
				_private.onCurrentLabelClicked(e);
				$(this).toggleClass( 'selectOpen', 'Remove' );
			});
			this.SELECTOR.find("ul a").on("click", function(e) {_private.onOptionClicked(e);});

			this.SELECTOR.find(".scrollable").on("scroll", function(e){_private.createScrollbar(e);})
			this.SELECTOR.find(".global-dropdown .content").on("mouseleave", function(e){_private.onDocumentClicked(e);})
			
			this.SELECTOR.find(".scrollbartop").on("click", function(e){_private.scrollContent(e, "up"); });
			this.SELECTOR.find(".scrollbarbottom").on("click", function(e){ _private.scrollContent(e, "down"); });
			
		},

		//------------------------------------------------
		// Show dropdown options
		//------------------------------------------------
		openDropdown: function() {
			this.SELECTOR.addClass("global-dropdown-open");
			this.SELECTOR.find(".content").css("height", "auto").show();
			this.open = true;
		},

		//------------------------------------------------
		// Hide dropdown options
		//------------------------------------------------
		closeDropdown: function() {
			$(document).off("click");
			this.SELECTOR.find(".scrollable").scrollTop(0);
			if (this.scrollbar) this.scrollbar.reset();
			this.SELECTOR.removeClass("global-dropdown-open");
			this.SELECTOR.find('.current').removeClass("selectOpen");
			this.SELECTOR.find(".content").css("height", 0).hide();
			this.open = false;
		},

		//------------------------------------------------
		// Open or close dropdown
		//------------------------------------------------
		onCurrentLabelClicked: function(e) {
			if (!this.open) {
				this.openDropdown();
			} else {
				this.closeDropdown();
			}
			$(document).one("click", function(e) {_private.onDocumentClicked(e);});
			e.stopPropagation();
			e.preventDefault();
		},

		//------------------------------------------------
		// Close dropdown
		//------------------------------------------------
		onDocumentClicked: function(e) {
			this.closeDropdown();
			e.stopPropagation();
			e.preventDefault();
		},

		//------------------------------------------------
		// Close dropdown and change active choice
		//------------------------------------------------
		onOptionClicked: function(e) {
			var optionID = this.SELECTOR.find("ul a").index(e.currentTarget);

			this.SELECTOR.find("ul a").removeClass("active");
			$(e.currentTarget).addClass("active");
			this.SELECTOR.find(".current").html($(e.currentTarget).html());
			this.FORM_SELECTOR.find("option").removeAttr("selected");
			this.FORM_SELECTOR.find("option").eq(optionID).attr("selected", "selected");
			
			this.closeDropdown();
			var newValue = this.SELECTOR.find(".global-dropdown li a").eq(optionID).attr("href");
			var optionLabel = this.SELECTOR.find(".global-dropdown li a").eq(optionID).html();

			e.stopPropagation();
			e.preventDefault();
		}
	};

	_private.init();
	return _public;
};