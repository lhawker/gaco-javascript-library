////////////////////////////////////////////////////////////////////////////////
// GACO Library
// Uses the Web Audio API
// Microphone Audio Visualization
// https://developer.mozilla.org/en-US/docs/Web/API/Web_Audio_API
// HELP LINKS USED IN DEVELOPMENT:
// Could be good to have a read of: http://chimera.labs.oreilly.com/books/1234000001552/ch05.html
// http://0xfe.blogspot.co.uk/2011/08/web-audio-spectrum-analyzer.html
// http://books.google.co.uk/books?id=6U7Y1gCi-5IC&pg=PA55&lpg=PA55&dq=requestAnimationFrame+audio+play+back&source=bl&ots=inEnKpHDVW&sig=2rzYs5Z__w2g8WaXoWOeQxK9if8&hl=en&sa=X&ei=jN0WVKe1DKmw7Aa2m4GoBQ&ved=0CEIQ6AEwBA#v=onepage&q=requestAnimationFrame%20audio%20play%20back&f=false
// helpful api info http://creativejs.com/resources/web-audio-api-getting-started
//http://updates.html5rocks.com/2012/09/Live-Web-Audio-Input-Enabled
//http://webaudiodemos.appspot.com/AudioRecorder/js/main.js
//http://typedarray.org/from-microphone-to-wav-with-getusermedia-and-web-audio/
// http://www.html5rocks.com/en/tutorials/getusermedia/intro
// https://github.com/cwilso/Audio-Input-Effects/tree/master/js/visualizer
//https://webaudiodemos.appspot.com/input/index.html
////////////////////////////////////////////////////////////////////////////////
var GACO = GACO || {};

GACO.MicrophoneVisualization = function (selector) {
	var _public = {
		//-------------------------------------------
		// Public stop button
		//-------------------------------------------
		IS_PLAYING: function(e){
			_private.isPlaying();
		},

		PLAY: function(e){
			_private.start();
		},

		STOP: function(e){
			_private.stop();
		},
	};

	var _private = {
		//-------------------------------------------
		// Variables
		//-------------------------------------------
		CONTROL_SELECTOR: null,
		HAS_CONSOLE: null,
		VIDEO_SOURCE: null,
		ID_PREFIX: "fb-",
		audioContext: null,
		analyser: null,
		frequencyData: null,
		frequencyBars: null,
		frequencyBarWidth: 2,
		mediaStreamSource: null,
		running: null,
		showVideo: false,
		showfrequencyDataArray: false,
		
		//-------------------------------------------
		// Init
		//-------------------------------------------
		init: function() {
			this.AudioContext = window.AudioContext || window.webkitAudioContext || window.mozAudioContext;
			this.CONTROL_SELECTOR = document.getElementById(selector);
			this.VIDEO_SOURCE = document.querySelector("video");
			this.HAS_CONSOLE = false;
			this.running = true;
			try {
				this.audioContext = new this.AudioContext();
			
				if(!this.showVideo) {
					this.VIDEO_SOURCE.style.display = "none";
				}
				
				// Call functions
				this.checkForConsole();
				this.createListeners();
				this.hasGetUserMedia();
			}
			catch(e) {
				_private.audioContextError();
  			}
		},

		//-------------------------------------------
  		// Check for Console support
  		//-------------------------------------------
  		checkForConsole: function() {
  			if(typeof console === "object"){
  				this.HAS_CONSOLE = true;
  			}
  		},

		//-------------------------------------------
  		// Create Event Listeners
  		//-------------------------------------------
		createListeners: function(){
			this.CONTROL_SELECTOR.querySelector(".stop").addEventListener("click", function(e){
				_private.stop();
			});

			this.CONTROL_SELECTOR.querySelector(".play").addEventListener("click", function(e){
				_private.start();
			});
		},

		//-------------------------------------------
		// Check if is playing
		//-------------------------------------------
		isPlaying: function(e) {},

		//-------------------------------------------
		// Start audio playback 
		//-------------------------------------------
		start: function(){
			if(this.VIDEO_SOURCE !== null){
				_private.VIDEO_SOURCE.play();
				_private.running = true;
			}
		},

		//-------------------------------------------
		// Stop audio playback 
		//-------------------------------------------
		stop: function(){
			if(this.VIDEO_SOURCE !== null){
				_private.VIDEO_SOURCE.pause();
				_private.running = false;
			}
		},

		//-------------------------------------------
		// Check browser getUserMedia support
		//-------------------------------------------
		hasGetUserMedia: function() {
  			var getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;
  			
			if (getUserMedia) {
				if(this.HAS_CONSOLE) {
					console.log("Browser has getUserMedia support.");
				}
				this.createVideoAudioStram(getUserMedia);
			}
		},

		//-------------------------------------------
		// Create a video audio stream
		// if we also need video set option to true.
		//-------------------------------------------
		createVideoAudioStram: function(getUserMedia){
			if(this.VIDEO_SOURCE !== null){
				getUserMedia.call(navigator, {audio: true, video: false}, function(stream) { 
				  		_private.VIDEO_SOURCE.src = window.URL.createObjectURL(stream);
				  		_private.VIDEO_SOURCE.play();
				  		_private.gotStream(stream);
					}, function(err){console.log("ERROR: " + err.name + err.message);
				});
			}
		},

		//-------------------------------------------
		// Audio creation from stream.
		// success callback when requesting audio input stream
		//-------------------------------------------
		gotStream: function(stream) {
		    // creates a gain node
    		var volume = this.audioContext.createGain();

    		this.mediaStreamSource = this.audioContext.createMediaStreamSource(stream);
    		// connect the stream to the gain node
    		this.mediaStreamSource.connect(volume);
    		this.analyser = this.audioContext.createAnalyser();
    		this.analyser.fftSize = 1024; // Power of two numbers: 32, 64, 256, 512, 1024, 2048
    		// Connect it to the destination.
    		this.mediaStreamSource.connect( this.analyser );
			this.frequencyData = new Uint8Array(this.analyser.frequencyBinCount);
			_private.createAudioFrequencyBars();
			// render frames
		    requestAnimationFrame(_private.updateAnalysers.bind());
		},

		updateAnalysers: function() {
			_private.analyser.getByteFrequencyData(_private.frequencyData);
			// is playing.
			if(this.running) {
				_private.CONTROL_SELECTOR.querySelector(".currentTime").innerHTML = parseFloat(_private.VIDEO_SOURCE.currentTime).toFixed(2);
				// Check if we should show the frequency data array
				if(_private.showfrequencyDataArray){
					_private.CONTROL_SELECTOR.querySelector(".frequencyData p").innerHTML =  Array.prototype.slice.call(_private.frequencyData).join(",");
				}				
				_private.animateFrequencyBars(_private.frequencyData, _private.frequencyBars);
			}
			
			_private.animationFrameReq();
		},

		//-------------------------------------------
		// Frame animation request
		//-------------------------------------------
		animationFrameReq: function(){
			requestAnimationFrame(_private.updateAnalysers.bind(this));
		},

		//-------------------------------------------
		// Create audio frequency bars
		//-------------------------------------------
		createAudioFrequencyBars: function(){
			if(_private.frequencyData.length > 0) {
			var displayWidth = _private.frequencyBarWidth * _private.frequencyData.length
			_private.CONTROL_SELECTOR.querySelector(".frequencyDataTwo").style.width = displayWidth;

				for (var i = 0; i < _private.frequencyData.length; i++) {
					var node = document.createElement("span");
					var id = this.ID_PREFIX+i;
					node.setAttribute("id",id);
					node.style.width = _private.frequencyBarWidth + "px";
					node.innerHTML = "&nbsp;";
					
					if(i !== 0) {
						node.style.left = (_private.frequencyBarWidth * i) + "px";
					}

					this.CONTROL_SELECTOR.querySelector(".frequencyDataTwo").appendChild(node);
				}

				this.frequencyBars = _private.CONTROL_SELECTOR.querySelectorAll(".frequencyDataTwo span");
			}
		},

		//-------------------------------------------
		// Animate the frequency bars
		//-------------------------------------------
		animateFrequencyBars: function(liveFrequencyData, frequencyBars){
			if(liveFrequencyData.length > 0 && frequencyBars.length > 0){
				if(liveFrequencyData.length === frequencyBars.length){
					for (var i = 0; i < frequencyBars.length; i++) {
						frequencyBars[i].style.height =  liveFrequencyData[i]+"px";
					}
				}
			}
		},

		//-------------------------------------------
		// AudioContext not supported error handling
		//-------------------------------------------
		audioContextError: function(){
			alert("Web Audio API is not supported in this browser");
		},

	};

	_private.init();
	return _public;
};