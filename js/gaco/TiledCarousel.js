////////////////////////////////////////////////////////////////////////////////
// GACO Library
// Tiled Carousel
////////////////////////////////////////////////////////////////////////////////
var GACO = GACO || {};

GACO.TiledCarousel = function(selector) {

	"use strict";

	var _public = new GACO.EventDispatcher();

	var _private = {
		//------------------------------------------------
		// Variables
		//------------------------------------------------
		SELECTOR: null,
		ANIMATION_SPEED: 500,
		MOVERIGHT: "swipeRight",
		MOVELEFT: "swipeLeft",
		ORDERBEFORE: "before",
		ORDERAFTER: "after",
		carouselItemsArray: null,
		totalNumberOfCarouselItems: null,
		immediate: true,
		leftIndent: null,
		order: null,

		//------------------------------------------------
		// Init
		//------------------------------------------------
		init: function() {
			this.SELECTOR = GACO.Utils.getDOMElement(selector);
			this.carouselItemsArray = this.SELECTOR.querySelectorAll(".carousel-inner li");
			this.totalNumberOfCarouselItems = this.carouselItemsArray.length;
			this.SELECTOR.querySelector(".carousel-navigation").style.display="inline-block";
			// call functions
			this.setCarouselInnerWidth();
			this.setCarouselSlideWidth();
			this.setOffsetMarginValue();
			this.setCarouselItemsOpacity();
			this.reorderCarouselItems();
			this.setNavigationPosition();
			this.offScreenFadeItems();
			this.createListeners();
			this.onResizeWindow();
		},


		//------------------------------------------------
		// Set the width of the slider within the carousel
		//------------------------------------------------
		setCarouselInnerWidth: function() {
			var newWidth = (this.SELECTOR.offsetWidth * this.totalNumberOfCarouselItems) + "px";
			this.SELECTOR.querySelector(".carousel-inner ul").style.width = newWidth;
		},

		//------------------------------------------------
		// Set the width of each slide
		//------------------------------------------------
		setCarouselSlideWidth: function() {
			var newWidth = Math.round(this.SELECTOR.offsetWidth - ((this.SELECTOR.offsetWidth / 10) * 2)) + "px";
			if(this.carouselItemsArray.length > 0){
				for (var i = 0; i < this.carouselItemsArray.length; i++) { 
    				this.carouselItemsArray[i].style.width = newWidth;
				}
			}
		},

		//------------------------------------------------
		// Reorder carousel items
		//------------------------------------------------
		reorderCarouselItems: function(order) {
			
			if(this.immediate){
				this.immediate = false;
				if(this.carouselItemsArray.length >=2) {
					this.carouselItemsArray[this.carouselItemsArray.length-1].parentNode.insertBefore(this.carouselItemsArray[this.carouselItemsArray.length-1], this.carouselItemsArray[0]);
				}
			}

			if(order === this.ORDERBEFORE){
				$(this.SELECTOR).find(".carousel-inner ul li:first").before($(this.SELECTOR).find(".carousel-inner ul li:last")); 
			}

			if(order === this.ORDERAFTER) {
				$(this.SELECTOR).find(".carousel-inner ul li:last").after($(this.SELECTOR).find(".carousel-inner ul li:first")); 
			}

			if(order === this.ORDERAFTER || order === this.ORDERBEFORE) {
				// set margin left back to default
				this.SELECTOR.querySelector(".carousel-inner ul").style.marginLeft = "-" + Math.round(this.SELECTOR.offsetWidth - ((this.SELECTOR.offsetWidth / 10)*3)) + "px";
				this.offScreenFadeItems();
			}

		},

		//------------------------------------------------
		// Fade off screen items
		//------------------------------------------------
		offScreenFadeItems: function() {
			var items = this.SELECTOR.querySelectorAll(".carousel-inner li");
    
		    if(items.length >= 3) {
		      for (var i = 0; i < items.length; i++) {
		          $(items[i]).stop().animate({"opacity" : 1},this.ANIMATION_SPEED,function(){});
		          $(items[0]).stop().animate({"opacity" : 0.3},this.ANIMATION_SPEED,function(){});
		          $(items[2]).stop().animate({"opacity" : 0.3},this.ANIMATION_SPEED,function(){});
		      }
		    }

		},

		//------------------------------------------------
		// Set margin offset value
		//------------------------------------------------
		setOffsetMarginValue: function() {
			var marginLeft = "-" + Math.round(this.SELECTOR.offsetWidth - ((this.SELECTOR.offsetWidth / 10)*3)) + "px";
			this.SELECTOR.querySelector(".carousel-inner ul").style.marginLeft=marginLeft;
		},

		//------------------------------------------------
		// Window resized, scale carousel
		//------------------------------------------------
		onResizeWindow: function() {
			this.setCarouselInnerWidth();
			this.setCarouselSlideWidth();
			this.setOffsetMarginValue();
			this.setNavigationPosition();
		},

		//------------------------------------------------
		// Set the current item opacity
		//------------------------------------------------
		setCarouselItemsOpacity: function(){
			if(this.immediate){
				if(this.carouselItemsArray.length > 0){
					this.carouselItemsArray[0].className = "active";
				}
			}
		},

		//------------------------------------------------
		// Set screen position on navigation
		//------------------------------------------------
		setNavigationPosition: function(){
			var top = "-" + (Math.round(this.SELECTOR.offsetHeight)) + "px",
				elArray = this.SELECTOR.querySelectorAll(".carousel-navigation li div");

			this.SELECTOR.querySelector(".carousel-navigation ul").style.top=top;
			
			if(elArray.length > 0){
				for (var i = 0; i < elArray.length; i++) { 
    				elArray[i].style.height = Math.round(this.SELECTOR.offsetHeight)+"px";
    				if(this.carouselItemsArray.length > 0){
						
						this.carouselItemsArray = this.SELECTOR.querySelectorAll(".carousel-inner li");

						var l = Math.round(this.carouselItemsArray[1].offsetLeft - (Math.round(this.SELECTOR.offsetWidth - ((this.SELECTOR.offsetWidth / 10)*3)))),
							r = Math.round(this.SELECTOR.offsetWidth - (this.SELECTOR.offsetWidth / 10));
						
						elArray[0].parentNode.style.left = (r - 1) + "px";
						elArray[1].parentNode.style.left = (l - elArray[0].parentNode.offsetWidth) +"px";

					}
				}
			}
		},

		//-------------------------------------------
  		// Carousel direction to animation
  		//-------------------------------------------
  		carouselAnimationDirection: function(e, direction){
  			var li = this.SELECTOR.querySelector(".carousel-inner li").offsetWidth;

		    if(direction === this.MOVERIGHT) {
		     this.leftIndent= parseInt(this.SELECTOR.querySelector(".carousel-inner ul").style.marginLeft) - li;
			 this.order = this.ORDERAFTER;
		    } else if (direction === this.MOVELEFT) {
		      this.leftIndent = parseInt(this.SELECTOR.querySelector(".carousel-inner ul").style.marginLeft) + li;
		      this.order = this.ORDERBEFORE;
		    }

		    // animate < think of a way to do this without jquery
		    $(this.SELECTOR).find(".carousel-inner ul:not(:animated)").stop().animate({"margin-left" : this.leftIndent},this.ANIMATION_SPEED,function(){ 
       			_private.reorderCarouselItems(_private.order);
    		});

  		},

  		//------------------------------------------------
  		// Move after key down events
  		//------------------------------------------------
  		keyEventNavigation: function(e){
  			if(e.keyCode === 39) {
  				_private.carouselAnimationDirection(e, this.MOVERIGHT); 
  			}
  			if(e.keyCode === 37) {
  				_private.carouselAnimationDirection(e, this.MOVELEFT); 
  			}
  		},

		//------------------------------------------------
		// Listen for interaction
		//------------------------------------------------
		createListeners: function() {
			    
			window.addEventListener("resize", function(e) {_private.onResizeWindow();});

			window.addEventListener("keydown", function(e) { _private.keyEventNavigation(e);});

			this.SELECTOR.querySelector(".right_scroll").addEventListener("click", function(e) { 
	        	_private.carouselAnimationDirection(e, _private.MOVERIGHT); 
	      	});

	      	this.SELECTOR.querySelector(".left_scroll").addEventListener("click", function(e) { 
	        	_private.carouselAnimationDirection(e, _private.MOVELEFT); 
	      	});
		}
	};

	_private.init();
	return _public;
};