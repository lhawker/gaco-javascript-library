////////////////////////////////////////////////////////////////////////////////
// Development lib Website
// Requires: gaco Libs, jQuery
////////////////////////////////////////////////////////////////////////////////
GACO.Utils.createNamespace("PROJECTNAME.projectName.controllers");

PROJECTNAME.projectName.controllers.MainController = function() {

	var _public = {

	}

	var _private = {
		//------------------------------------------------
		// Variables
		//------------------------------------------------
		globalView: null,

		//------------------------------------------------
		// Init
		//------------------------------------------------
		init: function() {
			this.createGlobalView();
		},

		//------------------------------------------------
		// Controls site-wide plugins and stuff
		//------------------------------------------------
		createGlobalView: function() {
			this.globalView = new PROJECTNAME.projectName.views.GlobalView();
		},

	};

	_private.init();
	return _public;

};

//------------------------------------------------
// Start controller when ready
//------------------------------------------------
if (PROJECTNAME.projectName.views) {
	PROJECTNAME.projectName.application = new PROJECTNAME.projectName.controllers.MainController();
} else {
	$(document).ready(function(e) {
		if (!PROJECTNAME.projectName.application) PROJECTNAME.projectName.application = new PROJECTNAME.projectName.controllers.MainController();
	});
}