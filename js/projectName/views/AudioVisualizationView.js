////////////////////////////////////////////////////////////////////////////////
// Development lib Website
// Google Map View functions
////////////////////////////////////////////////////////////////////////////////
GACO.Utils.createNamespace("PROJECTNAME.projectName.views");

PROJECTNAME.projectName.views.AudioVisualizationView = function (selector, audio) {

	var _public = {};

	var _private = {
		//-------------------------------------------
		// Variables
		//-------------------------------------------
		SELECTOR: null,
		AUDIO: null,
		AVInstance: null,
		
		//-------------------------------------------
		// Init
		//-------------------------------------------
		init: function () {
			this.SELECTOR = selector;
			this.AUDIO = audio;
			this.createListeners();
			this.AVInstance = new GACO.AudioVisualization(this.SELECTOR, this.AUDIO);
		},

		//-------------------------------------------
		// Create Event Listeners
		//-------------------------------------------
		createListeners: function () {
		}
	};

	_private.init();
	return _public;

};