////////////////////////////////////////////////////////////////////////////////
// Development lib Website
// Playing with WebGL
////////////////////////////////////////////////////////////////////////////////
GACO.Utils.createNamespace("PROJECTNAME.projectName.views");

PROJECTNAME.projectName.views.WebGlView = function (selector) {

	var _public = {};

	var _private = {
		//-------------------------------------------
		// Variables
		//-------------------------------------------
		SELECTOR: null,
		CAMERA: null,
		SCENE: null,
		RENDERER: null,
		POINT_LIGHT: null,
		CAMERA_CONTROLS: null,
		ctx: null,
		sphere: null,
		
		//-------------------------------------------
		// Init
		//-------------------------------------------
		init: function () {
			this.SELECTOR = selector;
			this.SCENE = new THREE.Scene();
			this.createListeners();
			this.initializeWebGl();
			this.cameraControls();
		},

		//-------------------------------------------
		// Create Event Listeners
		//-------------------------------------------
		createListeners: function () {
			window.addEventListener("keydown", function(e) { 
				if(e.keyIdentifier === "left" || e.keyIdentifier === "right"){
					e.preventDefault();
					e.stopPropagation();
				}
				_private.cameraControls(e.keyIdentifier);
				//http://mikeheavers.com/main/code-item/webgl_circular_camera_rotation_around_a_single_axis_in_threejs
			});
		},

		//-------------------------------------------
		// Initialize the GL context
		//-------------------------------------------
		initializeWebGl: function(){
			//this.gl = initWebGL(this.SELECTOR);
			if (window.WebGLRenderingContext) {
  				// check browser supports WebGL
  				if(typeof console === "object"){
  					console.log("browser supports WebGL");
  				};
  				
  				this.ctx = this.SELECTOR[0].getContext("webgl") || this.SELECTOR[0].getContext("experimental-webgl");
    			
    			if(!this.ctx) {
    				window.location = "http://get.webgl.org/troubleshooting";
    			} else {
    				this.RENDERER = new THREE.WebGLRenderer({ canvas: this.SELECTOR[0], context: this.ctx });
    				this.test();
    				this.createCamera();
    				this.createPointLighting();
    				this.creatSphere();

    				// draw scene!
    				if(this.SCENE && this.CAMERA && this.POINT_LIGHT) {
    					// Add lights and cameras to the scene
    					this.addItemsToScene(this.CAMERA);
    					this.addItemsToScene(this.POINT_LIGHT);
    				}

    				if(this.RENDERER){
    					//this.RENDERER.render(this.SCENE, this.CAMERA);
    					this.update();
    				}
    			} 

			} else {
				alert("Unable to initialize WebGL. Your browser may not support it.");
				alert("More information here: http://www.khronos.org/webgl/wiki/BlacklistsAndWhitelists#Chrome_on_Mac_OS_X");
			}
		},

		//-------------------------------------------
		// Test canvas gl
		//-------------------------------------------
		test: function(){
			if(this.RENDERER.context) {
				this.RENDERER.context.clearColor(0.0, 0.0, 0.0, 1.0);     // Set clear color to black, fully opaque
    			this.RENDERER.context.enable(this.ctx.DEPTH_TEST);        // Enable depth testing
    			this.RENDERER.context.depthFunc(this.ctx.LEQUAL);         // Near things obscure far things
    			this.RENDERER.context.clear(this.ctx.COLOR_BUFFER_BIT|this.ctx.DEPTH_BUFFER_BIT);
				this.RENDERER.context.viewport(0, 0, this.SELECTOR[0].width, this.SELECTOR[0].height);  // Clear the color as well as the depth buffer.
			}
		},

		//-------------------------------------------
		//  Create Camera
		//-------------------------------------------
		createCamera: function(){
			var VIEW_ANGLE = 45,
				ASPECT = this.SELECTOR[0].width / this.SELECTOR[0].height,
				NEAR = 0.1,
				FAR = 10000;
			this.CAMERA = new THREE.PerspectiveCamera( VIEW_ANGLE, ASPECT, NEAR, FAR );
			//this.CAMERA = new THREE.OrthographicCamera( VIEW_ANGLE, ASPECT, NEAR, FAR );

			this.CAMERA.position.z = 300;
		},

		//-------------------------------------------
		//  Create Point Light
		//-------------------------------------------
		createPointLighting: function(){
			this.POINT_LIGHT = new THREE.PointLight( 0xFFFFFF );
			this.POINT_LIGHT.position.x = 10;
			this.POINT_LIGHT.position.y = 50;
			this.POINT_LIGHT.position.z = 130;

			//light.position.set( -1, -1, -1 ).normalize(); look into normalize
		},

		//-------------------------------------------
		//  3D Sphere Shape
		//-------------------------------------------
		creatSphere: function(){
			// NOTE to self could loop array and add items here!
			var radius = 50, segments = 60, rings = 60;
			var sphereMaterial = new THREE.MeshLambertMaterial({ color: 0xCC0000, wireframe: true});

			var material = new THREE.ShaderMaterial( {
				vertexShader: document.getElementById( 'vertexShader' ).textContent,
				fragmentShader: document.getElementById( 'fragmentShader' ).textContent
			});

			//this.sphere = new THREE.Mesh(new THREE.SphereGeometry(radius, segments, rings), sphereMaterial);
			this.sphere = new THREE.Mesh(new THREE.SphereGeometry(radius, segments, rings), material);
			this.sphere.name='Sphere';
			this.addItemsToScene(this.sphere);

			//console.log(document.getElementById( 'vertexShader' ).textContent);
		},

		//-------------------------------------------
		//  Add controls
		//-------------------------------------------
		cameraControls: function(direction){
			var x = this.CAMERA.position.x,
        		y = this.CAMERA.position.y,
        		z = this.CAMERA.position.z,
        		rotSpeed = .02;

        		//toLowerCase()
        		if(direction) {
        			if (direction.toLowerCase() === "left"){ 
        				this.CAMERA.position.x = x * Math.cos(rotSpeed) + z * Math.sin(rotSpeed);
        				this.CAMERA.position.z = z * Math.cos(rotSpeed) - x * Math.sin(rotSpeed);
	    			} else if (direction.toLowerCase() === "right"){
	        			this.CAMERA.position.x = x * Math.cos(rotSpeed) - z * Math.sin(rotSpeed);
	        			this.CAMERA.position.z = z * Math.cos(rotSpeed) + x * Math.sin(rotSpeed);
	    			}
        		}
        	//this.CAMERA.lookAt(this.SCENE.position);

			//this.CAMERA.position.y = y;
			this.update();

		},


		update: function(){
			this.RENDERER.render(this.SCENE, this.CAMERA);
		},

		//-------------------------------------------
		//  Add all items to the scene
		//-------------------------------------------
		addItemsToScene: function(objs){
			this.SCENE.add(objs);
		},

		ch: function(){
			this.RENDERER.render(this.SCENE, this.CAMERA);
		},

	};

	_private.init();
	return _public;

};
// Fragment Shaders or Pixel Shaders: 
// http://threejs.org/examples/#webgl_shaders_ocean

// http://www.airtightinteractive.com/2013/02/intro-to-pixel-shaders-in-three-js/
// links which could be of help:
// http://stackoverflow.com/questions/22751313/what-is-the-difference-between-getcontextwebgl-vs-getcontext3d
// http://www.khronos.org/webgl/wiki/FAQ#What_is_the_recommended_way_to_initialize_WebGL.3F

// Camra stuff:
// http://stackoverflow.com/questions/7646124/creating-a-3d-free-camera-in-webgl-why-do-neither-of-these-methods-work

// THREE.JS http://aerotwist.com/tutorials/getting-started-with-three-js/