////////////////////////////////////////////////////////////////////////////////
// Development lib Website
// Global view for scripts used throughout the site
////////////////////////////////////////////////////////////////////////////////
GACO.Utils.createNamespace("PROJECTNAME.projectName.views");

PROJECTNAME.projectName.views.GlobalView = function () {
	var _public = {
		isTabletVersion: function () {
			return _private.tabletVersion;
		}
	};

	var _private = {

		//------------------------------------------------
		// Init
		//------------------------------------------------
		init: function() {
			this.homepageVideoPlayer();
			this.carousels();
			this.maps();
			this.selectBox();
			this.audioVisualization();
			this.microphoneAudio();
			this.webGl();
			this.Scrollbars();
		},

		//------------------------------------------------
		// Check for video and add view
		//------------------------------------------------
		homepageVideoPlayer: function() {
			if($("#homepage-video").length) {
				homepageVideoPlayer = new PROJECTNAME.projectName.views.VideoView(("#homepage-video"));
			}

			if($("#homepage-videoTwo").length) {
				homepageVideoPlayer = new PROJECTNAME.projectName.views.VideoView(("#homepage-videoTwo"));
			}
		},

		//------------------------------------------------
		// Add carousels
		//------------------------------------------------
		carousels: function() {
			if($("#main-carousel").length) {
				carouselOne = new PROJECTNAME.projectName.views.CarouselView(("#main-carousel"));

			}
		},

		//------------------------------------------------
		// Add maps
		//------------------------------------------------
		maps: function() {
			if($("#map-canvas").length) {
				mapOne = new PROJECTNAME.projectName.views.MapView(("#map-canvas"));
			}
		},

		//------------------------------------------------
		// Add custom selectBox
		//------------------------------------------------
		selectBox: function() {
			if($(".form-layout").length) {
				selectBox = new PROJECTNAME.projectName.views.CustomSelectBoxView((".form-layout"));

			}
		},

		//------------------------------------------------
		// Add audio visualization
		//------------------------------------------------
		audioVisualization: function(){
			if($("#r0audio").length) {
				AV = new PROJECTNAME.projectName.views.AudioVisualizationView("controls", "r0audio");
			}
		},

		//------------------------------------------------
		// Add Microphone Audio Visualization
		//------------------------------------------------
		microphoneAudio: function(){
			if($("#r1audio").length > 0) {
				MicrophoneAudioVisualization = new GACO.MicrophoneVisualization("controls");
			}
		},

		

		//------------------------------------------------
		// A view to play with webGL
		//------------------------------------------------
		webGl: function(){
			if($("#glcanvas").length > 0) {
				wGL = new PROJECTNAME.projectName.views.WebGlView($("#glcanvas"));
			}
		},

		//------------------------------------------------
		// A view for scrollbars
		//------------------------------------------------
		Scrollbars: function(){
			if($(".main_content").length > 0) {
				scrolls = new PROJECTNAME.projectName.views.ScrollbarsView((".main_content"));
			}
		},

	};

	_private.init();
	return _public;

};