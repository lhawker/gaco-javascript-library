////////////////////////////////////////////////////////////////////////////////
// Development lib Website
// Carousel View functions
////////////////////////////////////////////////////////////////////////////////
GACO.Utils.createNamespace("PROJECTNAME.projectName.views");

PROJECTNAME.projectName.views.CarouselView = function (selector) {

	var _public = {

	};

	var _private = {
		//-------------------------------------------
		// Variables
		//-------------------------------------------
		SELECTOR: null,
		carouselInstance: null,
		
		//-------------------------------------------
		// Init
		//-------------------------------------------
		init: function () {
			this.SELECTOR = $(selector);
			this.createListeners();
			this.carouselInstance = new GACO.TiledCarousel(selector);
		},

		//-------------------------------------------
		// Create Event Listeners
		//-------------------------------------------
		createListeners: function () {
		}
	};

	_private.init();
	return _public;
};