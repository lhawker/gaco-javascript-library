////////////////////////////////////////////////////////////////////////////////
// Development lib Website
// Custom Selectbox View functions
////////////////////////////////////////////////////////////////////////////////
GACO.Utils.createNamespace("PROJECTNAME.projectName.views");

PROJECTNAME.projectName.views.CustomSelectBoxView = function (selector) {

	var _public = {

	};

	var _private = {
		//-------------------------------------------
		// Variables
		//-------------------------------------------
		SELECTOR: null,
		ALLSELECTBOXES: null,
		
		//-------------------------------------------
		// Init
		//-------------------------------------------
		init: function () {
			this.SELECTOR = $(selector);
			this.createListeners();
			this.ALLSELECTBOXES = this.SELECTOR.find(".dumpSelectBoxHere");
			if(this.ALLSELECTBOXES.length > 0) {
				for (var i = 0; i < this.ALLSELECTBOXES.length; i++) {
			      var el = this.ALLSELECTBOXES[i];
			      this.selectBox = new GACO.CustomSelectBox(el, ".type-selection"+i, "selectBox"+i);
			    }
			}
		},

		//-------------------------------------------
		// Create Event Listeners
		//-------------------------------------------
		createListeners: function () {
		}
	};

	_private.init();
	return _public;
};