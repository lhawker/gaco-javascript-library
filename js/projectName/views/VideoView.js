////////////////////////////////////////////////////////////////////////////////
// Development lib Website
// Home video functions
////////////////////////////////////////////////////////////////////////////////
GACO.Utils.createNamespace("PROJECTNAME.projectName.views");

PROJECTNAME.projectName.views.VideoView = function (selector) {

	var _public = {

	};

	var _private = {
		//-------------------------------------------
		// Variables
		//-------------------------------------------
		SELECTOR: null,
		videoPlayer: null,
		IS_MOBILE: (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)),
		
		//-------------------------------------------
		// Init
		//-------------------------------------------
		init: function () {
			this.SELECTOR = $(selector);
			this.createListeners();
			this.createVideoPlayer();
			this.videoPlaceholderOverlayBackground();
			this.mobileDefaultPlayer();
			this.checkingVideoComplete();
		},

		//-------------------------------------------
		// Create Event Listeners
		//-------------------------------------------
		createListeners: function () {
			this.SELECTOR.find("#close_btn").on("click", function(e) { 
				_private.closeVideoPlayer(this);
				_private.pauseResetPlaybackHeader();
			});
			this.SELECTOR.find(".large-play-button").on("click", function(e) { 
				_private.openVideoPlayer();
				_private.videoPlay(); 
			});


		},

		//-------------------------------------------
		// Placeholder overlay background image
		//-------------------------------------------
		videoPlaceholderOverlayBackground: function() {
			var imgURL = this.SELECTOR.find(".video_placeholder_image").data("background");
			this.SELECTOR.find(".video_placeholder_image").css(
				{
					"background" : 'url("' +imgURL+ '")',
					"-webkit-background-size": "cover", 
					"-moz-background-size": "cover", 
					"-o-background-size": "cover", 
					"background-size": "cover",
					"background-position": "center center"
				}
			);

		},

		//-------------------------------------------
		// Create a youtube video player
		//-------------------------------------------
		createVideoPlayer: function(e) {
			this.videoPlayer = new PROJECTNAME.projectName.views.global.video.VideoPlayer(this.SELECTOR.find(".video-player"), true);
		},

		//-------------------------------------------
		// Hide / close video player
		//-------------------------------------------
		closeVideoPlayer: function(e) {
			this.SELECTOR.find(".video_placeholder_image").show();
			this.SELECTOR.find("#video_overlay_container").stop().animate({"opacity":0}, 400);
		},

		//-------------------------------------------
		// Show the video
		//-------------------------------------------
		openVideoPlayer: function(e) {
			this.SELECTOR.find(".video_placeholder_image").hide();
			this.SELECTOR.find("#video_overlay_container").css("display","block").stop().animate({"opacity":1}, 400);
		},

		//-------------------------------------------
		// Pause and reset playback to 0
		//-------------------------------------------
		pauseResetPlaybackHeader: function() {
			this.videoPlayer.pauseVideo();
		},

		//-------------------------------------------
		// Play the video
		//-------------------------------------------
		videoPlay: function() {
			if(!this.IS_MOBILE) {
				this.videoPlayer.play();
			} else {
				if (this.videoPlayer.getHasPlayTriggered()) {
					this.videoPlayer.play();
				}
				this.SELECTOR.find(".controls-container").hide();
			}
		},

		//-------------------------------------------
		// if mobile show default player
		//-------------------------------------------
		mobileDefaultPlayer: function() {
			/*if(this.IS_MOBILE) {
				this.openVideoPlayer();
				this.SELECTOR.find(".controls-container").hide();
				this.SELECTOR.find("#close_btn").hide();
				this.SELECTOR.find(".video-mask").hide();
				
			}*/
		},

		//---------------------------------------------
		// keep checking for video Complete message
		//---------------------------------------------
		checkingVideoComplete: function(){
			setInterval(function(e){
				if(_private.videoPlayer.isComplete()) {
					_private.SELECTOR.find(".video_placeholder_image").show();
					_private.SELECTOR.find("#video_overlay_container").stop().animate({"opacity":0}, 400);
				}
			}, 1000);
		},
		
		
	};

	_private.init();
	return _public;

};