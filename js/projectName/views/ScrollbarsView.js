////////////////////////////////////////////////////////////////////////////////
// Development lib Website
// Scrollbars View functions
////////////////////////////////////////////////////////////////////////////////
GACO.Utils.createNamespace("PROJECTNAME.projectName.views");

PROJECTNAME.projectName.views.ScrollbarsView = function (selector) {

	var _public = {

	};

	var _private = {
		//-------------------------------------------
		// Variables
		//-------------------------------------------
		SELECTOR: null,
		aConts: [],
    	mouseY: 0,
    	N: 0,
    	asd: 0, /*active scrollbar element*/
    	sc: 0,
    	sp: 0,
    	to: 0,
    	scrollbarWidth: 20,
    	scrollHeight: null,
    	scrollOffsetHeight: null,
    	//ratioNum: null,

    	//-------------------------------------------
		// Init
		//-------------------------------------------
		init: function () {
			this.SELECTOR = GACO.Utils.getDOMElement(selector);
			this.scrollbar();
			this.createListeners();

		},

		//-------------------------------------------
		// Scollbar constructor
		//-------------------------------------------

	    scrollbar: function () {
	        var cont_clone = this.SELECTOR.cloneNode(false);

	        cont_clone.style.overflow = "hidden";
	        this.SELECTOR.parentNode.appendChild(cont_clone);
	        cont_clone.appendChild(this.SELECTOR);
	        this.SELECTOR.style.position = 'absolute';
	        this.SELECTOR.style.left = this.SELECTOR.style.top = '0px';
	        this.SELECTOR.style.width = this.SELECTOR.style.height = '100%';

	        // adding new container into array
	        this.aConts[this.N++] = this.SELECTOR;

	        this.SELECTOR.sg = false;
	        this.scrollHeight = this.SELECTOR.scrollHeight;
	        this.scrollOffsetHeight = this.SELECTOR.offsetHeight;
	        _private.ratioNum = (_private.scrollOffsetHeight - 2 *  _private.scrollbarWidth) / _private.scrollHeight;
	        


	        //creating scrollbar child elements
	        this.SELECTOR.st = this.create_div('ssb_st', this.SELECTOR, cont_clone);
	        this.SELECTOR.sb = this.create_div('ssb_sb', this.SELECTOR, cont_clone);
	        this.SELECTOR.su = this.create_div('ssb_up', this.SELECTOR, cont_clone);
	        this.SELECTOR.sd = this.create_div('ssb_down', this.SELECTOR, cont_clone);

	        // on mouse down on free track area - move our scroll element too
	        this.SELECTOR.st.onmousedown = function (e) {
	            if (! e) e = window.event;
	            _private.asd =  _private.SELECTOR;

	            _private.mouseY = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
	            for (var o =  _private.SELECTOR, y = 0; o != null; o = o.offsetParent) y += o.offsetTop;
	             _private.SELECTOR.scrollTop = (_private.mouseY - y - ( _private.SELECTOR.ratio *  _private.SELECTOR.offsetHeight / 2) -  _private.scrollbarWidth) /  _private.SELECTOR.ratio;
	        }



	        // onscroll - change positions of scroll element
	        this.SELECTOR._private_onscroll = function () {
 				this.ratio = _private.ratioNum;
 				this.sb.style.top = Math.floor( _private.scrollbarWidth + this.scrollTop * _private.ratioNum) + 'px';
	        }

	

	        // start scrolling
	        this.SELECTOR._private_onscroll();
	        
	        // new founction _private.onStartScrolling();

	        _private.refresh();
	        
	        // binding own onscroll event
	        this.SELECTOR.onscroll =  this.SELECTOR._private_onscroll;
	       // return  _private.SELECTOR;
	    },

		//-------------------------------------------
		// Create Event Listeners
		//-------------------------------------------
		createListeners: function () {
			window.addEventListener("mousemove", function(e) { 
	        	_private.onmousemove();
	        });
	        window.addEventListener("mouseup", function(e) {  
	        	_private.onmouseup(); 
	        });
	        
	        window.addEventListener("resize", function(e) {  
	        	_private.refresh(); 
	        });

	        this.SELECTOR.parentNode.querySelector(".ssb_sb").addEventListener("mouseover", function(e) {  
	        	this.className = 'ssb_sb ssb_sb_over';
	        });

	        this.SELECTOR.parentNode.querySelector(".ssb_sb").addEventListener("mouseout", function(e) {  
	        	this.className = 'ssb_sb';
	        });

	        this.SELECTOR.parentNode.querySelector(".ssb_sb").addEventListener("mousedown", function(e) {  
	        	this.className = 'ssb_sb ssb_sb_down';
	        	if (!_private.SELECTOR.sg) {
	                	_private.asd = _private.SELECTOR;
	                	_private.SELECTOR.yZ = e.screenY;
	                	_private.SELECTOR.sZ = _private.SELECTOR.scrollTop;
	                	_private.SELECTOR.sg = true;
	            }
	        });

	        this.SELECTOR.parentNode.querySelector(".ssb_up").addEventListener("mousedown", function(e) {  
	        	_private.mousedown(this, -1); 
	        });

	        this.SELECTOR.parentNode.querySelector(".ssb_down").addEventListener("mousedown", function(e) {  
	        	_private.mousedown(this,  1); 
	        });
		},

		//-------------------------------------------
		// Create and append div finc
		//-------------------------------------------
	    create_div : function(c, cont, cont_clone) {
	        var o = document.createElement('div');
	        o.cont = cont;
	        o.className = c;
	        cont_clone.appendChild(o);
	        return o;
	    },

	    //-------------------------------------------
	    // Do clear of controls
	    //-------------------------------------------
	    clear : function () {
	        clearTimeout(_private.to);
	        _private.sc = 0;
	        return false;
	    },

	    onStartScrolling: function(){
	    	console.log("onStartScrolling");
	    	/*var ratio = (_private.scrollOffsetHeight - 2 *  _private.scrollbarWidth) / _private.scrollHeight;

	    	console.log(ratio);*/
	    },
	    //-------------------------------------------
	    // Refresh scrollbar
	    //-------------------------------------------
	    refresh : function () {
	        for (var i = 0, N = this.N; i < N; i++) {
	            var o = this.aConts[i];
	            console.log("_private.ratioNum " + _private.ratioNum);	            
	            o._private_onscroll();
	            o.sb.style.width = o.st.style.width = o.su.style.width = o.su.style.height = o.sd.style.width = o.sd.style.height = _private.scrollbarWidth + 'px';
	            o.sb.style.height = Math.ceil(Math.max(_private.scrollbarWidth * .5, _private.ratioNum * o.offsetHeight) + 1) + 'px';
	        }
	    },

	    //-------------------------------------------
	    // Arrow scrolling
	    //-------------------------------------------
	    arrow_scroll : function () {
	        if (_private.sc != 0) {
	            _private.asd.scrollTop += 6 * _private.sc / _private.ratioNum;
	            _private.to = setTimeout(_private.arrow_scroll, _private.sp);
	            _private.sp = 32;
	        }
	    },

	    //-------------------------------------------
	    // Scroll on mouse down
	    //-------------------------------------------
	    mousedown : function (o, s) {
	        if (_private.sc == 0) {
	            // new class name
	            o.cont.sb.className = 'ssb_sb ssb_sb_down';
	            _private.asd = o.cont;
	            _private.sc = s;
	            _private.sp = 400;
	            _private.arrow_scroll();
	        }
	    },

	    //-------------------------------------------
	    // On mouseMove binded event
	    //-------------------------------------------
	    onmousemove : function(e) {
	        if (! e) e = window.event;
	        // get vertical mouse position
	        _private.mouseY = e.screenY;
	        if (_private.asd.sg) _private.asd.scrollTop = _private.asd.sZ + (_private.mouseY - _private.asd.yZ) / _private.asd.ratio;
	    },

	    //-------------------------------------------
	    // on mouseUp binded event
	    //-------------------------------------------
	    onmouseup : function (e) {
	        if (! e) e = window.event;
	        var tg = (e.target) ? e.target : e.srcElement;
	        if (_private.asd && document.releaseCapture) _private.asd.releaseCapture();

	        // new class name
	        if (_private.asd) _private.asd.sb.className = (tg.className.indexOf('scrollbar') > 0) ? 'ssb_sb ssb_sb_over' : 'ssb_sb';
	        document.onselectstart = '';
	        _private.clear();
	        _private.asd.sg = false;
	    }
	};

	_private.init();
	return _public;
};