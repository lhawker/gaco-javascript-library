////////////////////////////////////////////////////////////////////////////////
// Development lib Website
// YouTube/Local video player
////////////////////////////////////////////////////////////////////////////////
GACO.Utils.createNamespace("PROJECTNAME.projectName.views.global.video");

PROJECTNAME.projectName.views.global.video.VideoPlayer = function(selector, controls, autoPlay, options, noControls) {

	"use strict";

	var _public = new GACO.video.BasicYouTubePlayer(selector, {controls: false, autoPlay: false, preventInit: false, loop: false, mute: false, annotations: false, modileMode: false}, autoPlay);
	//var _public = new GACO.video.StandardYouTubePlayer(selector, {controls: false, autoPlay: false, preventInit: false, loop: false, mute: false, annotations: false, modileMode: false}, autoPlay);

	//------------------------------------------------
	// Pause video player if not already playing
	//------------------------------------------------
	_public.play = function() {
		_private.onPlayClicked();
	};

	//------------------------------------------------
	// Play a new video (stops any currently playing)
	//------------------------------------------------
	_public.playNewVideo = function(autoPlay, videoURL, webmURL) {
		// TODO: Swap videos without rebuilding?
	};

	_public.stopVideo = function() {
		_private.onStopClicked();
	};

	//------------------------------------------------
	// Return boolean showing if video is currently playing
	//------------------------------------------------
	_public.isPlaying = function() {
		return _private.activePlayer.isPlaying();
	};

	_public.isComplete = function(){
		return _private.completed;
	};

	//------------------------------------------------
	// Return boolean showing if video has been played
	//------------------------------------------------
	// _public.getHasPlayTriggered = function() {
	// 	return this.getHasPlayTriggered();
	// };
				
	var _private = {
		//------------------------------------------------
		// Variables
		//------------------------------------------------
		controls: null,
		autoPlay: false,
		options: {},
		activePlayer: null,
		videoPlaying: false,
		hasPlayed: false,
		completed: false,

		//------------------------------------------------
		// Init
		//------------------------------------------------
		init: function() {
			this.SELECTOR = GACO.Utils.getDOMElement(selector);
			this.autoPlay = autoPlay;
			this.options = options;
			this.createControls();
			this.createListeners();
		},

		//------------------------------------------------
		// Check if is mobile
		//------------------------------------------------
		is_mobile: function(e) {
			if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
 				return true;
			} 
		},

		//------------------------------------------------
		// Creates basic controls
		//------------------------------------------------
		createControls: function() {
			if(!this.is_mobile()) {
				this.controls = new GACO.video.Controls(this.SELECTOR.querySelector(".controls-container"), this.controlsOptions);
			}
		},

		//------------------------------------------------
		// Listen for user interaction
		//------------------------------------------------
		createListeners: function() {
			
			if(!this.is_mobile()) {
				this.controls.addListener(this.controls.PLAY_CLICKED, function(e) {_private.onPlayClicked(e);});
				this.controls.addListener(this.controls.PAUSE_CLICKED, function(e) {_private.onPauseClicked(e);});
				this.controls.addListener(this.controls.MUTE_CLICKED, function(e) {_private.onMuteClicked(e);});
				this.controls.addListener(this.controls.UNMUTE_CLICKED, function(e) {_private.onUnMuteClicked(e);});
				this.controls.addListener(this.controls.PROGRESS_CLICKED, function(e) {_private.onProgressClicked(e);});

				this.controls.addListener(this.controls.HANDLE_MOUSE_DOWN, function(e) {_private.onHandleMouseDown(e);});
				this.controls.addListener(this.controls.HANDLE_DRAGGED, function(e) {_private.onHandleDragged(e);});
				this.controls.addListener(this.controls.HANDLE_RELEASED, function(e) {_private.onHandleReleased(e);});
				

				this.controls.addListener(this.controls.ENTER_FULL_SCREEN_CLICKED, function(e) {_private.onEnterFullScreenClicked(e);});
				this.controls.addListener(this.controls.EXIT_FULL_SCREEN_CLICKED, function(e) {_private.onExitFullScreenClicked(e);});

				
				_public.addListener(_public.PLAY_PROGRESS, function(e) {_private.onPlayProgress(e);});
				_public.addListener(_public.LOAD_PROGRESS, function(e) {_private.onLoadProgress(e);});
				_public.addListener(_public.VIDEO_COMPLETE, function(e) {_private.onVideoComplete(e);});
				_public.addListener(_public.VIDEO_PLAYING, function(e) {_private.onVideoPlaying(e);});
				_public.addListener(_public.VIDEO_PAUSED, function(e) {_private.onVideoPaused(e);});
				
			}
			

		},

		onHandleMouseDown: function(e) {
			
		},

		onHandleDragged: function(e) {

		},

		onHandleReleased: function(e) {
			var percentage = this.controls.getRequestedPercentage();
			var duration = _public.getDuration();

			_public.seekTo(percentage * duration);

			this.controls.setProgress(percentage);
			if (this.videoWasPlaying) {
				this.videoWasPlaying = false;
				_public.playVideo();
			}
		},

		onPlayClicked: function(e) {
			_public.playVideo();
			_private.completed = false;
		},

		onPauseClicked: function(e) {
			_public.pauseVideo();
		},

		onStopClicked: function(e) {
			_public.pauseVideo();
			_public.seekTo(0);
		},

		onMuteClicked: function(e) {
			_public.mute();
		},

		onUnMuteClicked: function(e) {
			_public.unMute();
		},

		onProgressClicked: function(e) {
			var percentage = this.controls.getRequestedPercentage();
			_public.seekTo(percentage * _public.getDuration());
			this.controls.setProgress(percentage);
		},

		onEnterFullScreenClicked: function(e) {
			_public.enterFullScreen();
		},

		onExitFullScreenClicked: function(e) {
			_public.exitFullScreen();
		},

		onPlayProgress: function(e) {
			var progress = _public.getPlayProgress();
			this.controls.setProgress(progress);
		},

		onLoadProgress: function(e) {
			var progress = _public.getVideoLoadedFraction();
			this.controls.setLoaded(progress);
		},

		onVideoComplete: function(e) {
			this.controls.setProgress(1);
			this.controls.showPlayButton();
			_private.completed = true;
		},

		onVideoPlaying: function(e) {
			this.controls.showPauseButton();
		},

		onVideoPaused: function(e) {
			this.controls.showPlayButton();
		}

	};

	_private.init();
	return _public;
};